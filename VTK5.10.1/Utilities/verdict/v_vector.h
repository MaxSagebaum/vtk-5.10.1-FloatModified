/*=========================================================================

  Module:    v_vector.h

  Copyright (c) 2006 Sandia Corporation.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/Copyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/


/*
 *
 * v_vector.h contains simple vector operations
 *
 * This file is part of VERDICT
 *
 */

// .SECTION Thanks
// Prior to its inclusion within VTK, this code was developed by the CUBIT
// project at Sandia National Laboratories. 

#ifndef VERDICT_VECTOR
#define VERDICT_VECTOR


#include "verdict.h"
#include <math.h>
#include <assert.h>


// computes the dot product of 3d vectors
//BT_DOUBLE dot_product( BT_DOUBLE vec1[], BT_DOUBLE vec2[] );

// computes the cross product
//BT_DOUBLE *cross_product( BT_DOUBLE vec1[], BT_DOUBLE vec2[], BT_DOUBLE answer[] = 0);

// computes the interior angle between 2 vectors in degrees
//BT_DOUBLE interior_angle ( BT_DOUBLE vec1[], BT_DOUBLE vec2[] );

// computes the length of a vector
//BT_DOUBLE length ( BT_DOUBLE vec[] );

//BT_DOUBLE length_squared (BT_DOUBLE vec[] );


inline BT_DOUBLE dot_product( BT_DOUBLE vec1[], BT_DOUBLE vec2[] )
{

  BT_DOUBLE answer =  vec1[0] * vec2[0] +
     vec1[1] * vec2[1] +
     vec1[2] * vec2[2];
  return answer;
}
inline void normalize( BT_DOUBLE vec[] )
{
  BT_DOUBLE x = sqrt( vec[0]*vec[0] +
             vec[1]*vec[1] +
             vec[2]*vec[2] );

  vec[0] /= x;
  vec[1] /= x;
  vec[2] /= x;

}


inline BT_DOUBLE * cross_product( BT_DOUBLE vec1[], BT_DOUBLE vec2[], BT_DOUBLE answer[] )
{
  answer[0] = vec1[1] * vec2[2] - vec1[2] * vec2[1];
  answer[1] = vec1[2] * vec2[0] - vec1[0] * vec2[2];
  answer[2] = vec1[0] * vec2[1] - vec1[1] * vec2[0];
  return answer;
}

inline BT_DOUBLE length ( BT_DOUBLE vec[] )
{
  return (sqrt ( vec[0] * vec[0] + vec[1] * vec[1] + vec[2] * vec[2] ));
}

inline BT_DOUBLE length_squared (BT_DOUBLE vec[] )
{
  return (vec[0] * vec[0] + vec[1] * vec[1] + vec[2] * vec[2] );
}


inline BT_DOUBLE interior_angle( BT_DOUBLE vec1[], BT_DOUBLE vec2[] )
{
  BT_DOUBLE len1, len2, cosAngle, angleRad;

  if (  ((len1 = length(vec1)) > 0 ) && ((len2 = length(vec2)) > 0 ) )
  {
    cosAngle = dot_product(vec1, vec2) / (len1 * len2);
  }
  else
  {
    assert(len1 > 0);
    assert(len2 > 0);
  }

  if ((cosAngle > 1.0) && (cosAngle < 1.0001))
  {
    cosAngle = 1.0;
    angleRad = acos(cosAngle);
  }
  else if (cosAngle < -1.0 && cosAngle > -1.0001)
  {
    cosAngle = -1.0;
    angleRad = acos(cosAngle);
  }
  else if (cosAngle >= -1.0 && cosAngle <= 1.0)
    angleRad = acos(cosAngle);
  else
  {
    assert(cosAngle < 1.0001 && cosAngle > -1.0001);
  }

  return( (angleRad * 180.) / VERDICT_PI );

}

#endif


