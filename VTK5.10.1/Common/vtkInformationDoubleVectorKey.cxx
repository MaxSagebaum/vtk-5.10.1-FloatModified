/*=========================================================================

  Program:   Visualization Toolkit
  Module:    vtkInformationDoubleVectorKey.cxx

  Copyright (c) Ken Martin, Will Schroeder, Bill Lorensen
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/Copyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
#include "vtkInformationDoubleVectorKey.h"

#include "vtkInformation.h" // For vtkErrorWithObjectMacro

#include <vector>


//----------------------------------------------------------------------------
vtkInformationDoubleVectorKey
::vtkInformationDoubleVectorKey(const char* name, const char* location,
                                 int length):
  vtkInformationKey(name, location), RequiredLength(length)
{
  vtkCommonInformationKeyManager::Register(this);
}

//----------------------------------------------------------------------------
vtkInformationDoubleVectorKey::~vtkInformationDoubleVectorKey()
{
}

//----------------------------------------------------------------------------
void vtkInformationDoubleVectorKey::PrintSelf(ostream& os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os, indent);
}

//----------------------------------------------------------------------------
class vtkInformationDoubleVectorValue: public vtkObjectBase
{
public:
  vtkTypeMacro(vtkInformationDoubleVectorValue, vtkObjectBase);
  std::vector<BT_DOUBLE> Value;
};

//----------------------------------------------------------------------------
void vtkInformationDoubleVectorKey::Append(vtkInformation* info, BT_DOUBLE value)
{
  vtkInformationDoubleVectorValue* v =
    performCast<vtkInformationDoubleVectorValue *>(
      this->GetAsObjectBase(info));
  if(v)
    {
    v->Value.push_back(value);
    }
  else
    {
    this->Set(info, &value, 1);
    }
}

//----------------------------------------------------------------------------
void vtkInformationDoubleVectorKey::Set(vtkInformation* info, BT_DOUBLE* value,
                                         int length)
{
  if(value)
    {
    if(this->RequiredLength >= 0 && length != this->RequiredLength)
      {
      vtkErrorWithObjectMacro(
        info,
        "Cannot store BT_DOUBLE vector of length " << length
        << " with key " << this->Location << "::" << this->Name
        << " which requires a vector of length "
        << this->RequiredLength << ".  Removing the key instead.");
      this->SetAsObjectBase(info, 0);
      return;
      }
    vtkInformationDoubleVectorValue* v =
      new vtkInformationDoubleVectorValue;
    this->ConstructClass("vtkInformationDoubleVectorValue");
    v->Value.insert(v->Value.begin(), value, value+length);
    this->SetAsObjectBase(info, v);
    v->Delete();
    }
  else
    {
    this->SetAsObjectBase(info, 0);
    }
}

//----------------------------------------------------------------------------
BT_DOUBLE* vtkInformationDoubleVectorKey::Get(vtkInformation* info)
{
  vtkInformationDoubleVectorValue* v =
    performCast<vtkInformationDoubleVectorValue *>(
      this->GetAsObjectBase(info));
  return (v && !v->Value.empty())?(&v->Value[0]):0;
}

//----------------------------------------------------------------------------
BT_DOUBLE vtkInformationDoubleVectorKey::Get(vtkInformation* info, int idx)
{
  if (idx >= this->Length(info))
    {
    vtkErrorWithObjectMacro(info,
                            "Information does not contain " << idx
                            << " elements. Cannot return information value.");
    return 0;
    }
  BT_DOUBLE* values = this->Get(info);
  return values[idx];
}

//----------------------------------------------------------------------------
void vtkInformationDoubleVectorKey::Get(vtkInformation* info,
                                     BT_DOUBLE* value)
{
  vtkInformationDoubleVectorValue* v =
    performCast<vtkInformationDoubleVectorValue *>(
      this->GetAsObjectBase(info));
  if(v && value)
    {
    for(std::vector<BT_DOUBLE>::size_type i = 0;
        i < v->Value.size(); ++i)
      {
      value[i] = v->Value[i];
      }
    }
}

//----------------------------------------------------------------------------
int vtkInformationDoubleVectorKey::Length(vtkInformation* info)
{
  vtkInformationDoubleVectorValue* v =
    performCast<vtkInformationDoubleVectorValue *>(
      this->GetAsObjectBase(info));
  return v?performCast<int>(v->Value.size()):0;
}

//----------------------------------------------------------------------------
void vtkInformationDoubleVectorKey::ShallowCopy(vtkInformation* from,
                                          vtkInformation* to)
{
  this->Set(to, this->Get(from), this->Length(from));
}

//----------------------------------------------------------------------------
void vtkInformationDoubleVectorKey::Print(ostream& os, vtkInformation* info)
{
  // Print the value.
  if(this->Has(info))
    {
    BT_DOUBLE* value = this->Get(info);
    int length = this->Length(info);
    const char* sep = "";
    for(int i=0; i < length; ++i)
      {
      os << sep << value[i];
      sep = " ";
      }
    }
}
