/*=========================================================================

  Program:   Visualization Toolkit
  Module:    vtkVector.h

  Copyright (c) Ken Martin, Will Schroeder, Bill Lorensen
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/Copyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/

// .NAME vtkRect - templated base type for storage of 2D rectangles.
//
// .SECTION Description
// This class is a templated data type for storing and manipulating rectangles.
// The memory layout is a contiguous array of the specified type, such that a
// BT_FLOAT[4] can be cast to a vtkRectf and manipulated. Also a BT_FLOAT[12] could
// be cast and used as a vtkRectf[3].

#ifndef __vtkRect_h
#define __vtkRect_h

#include "vtkVector.h"

template<typename T>
class vtkRect : public vtkVector<T, 4>
{
public:
  vtkRect()
  {
  }

  vtkRect(const T& x, const T& y, const T& width, const T& height)
  {
    this->Data[0] = x;
    this->Data[1] = y;
    this->Data[2] = width;
    this->Data[3] = height;
  }

  explicit vtkRect(const T* init) : vtkVector<T, 4>(init) { }

  // Description:
  // Set the x, y components of the rectangle, and the width/height.
  void Set(const T& x, const T& y, const T& width, const T& height)
  {
    this->Data[0] = x;
    this->Data[1] = y;
    this->Data[2] = width;
    this->Data[3] = height;
  }

  // Description:
  // Set the x component of the rectangle bottom corner, i.e. element 0.
  void SetX(const T& x) { this->Data[0] = x; }

  // Description:
  // Get the x component of the rectangle bottom corner, i.e. element 0.
  const T& GetX() const { return this->Data[0]; }
  const T& X() const { return this->Data[0]; }

  // Description:
  // Set the y component of the rectangle bottom corner, i.e. element 1.
  void SetY(const T& y) { this->Data[1] = y; }

  // Description:
  // Get the y component of the rectangle bottom corner, i.e. element 1.
  const T& GetY() const { return this->Data[1]; }
  const T& Y() const { return this->Data[1]; }

  // Description:
  // Set the width of the rectanle, i.e. element 2.
  void SetWidth(const T& width) { this->Data[2] = width; }

  // Description:
  // Get the width of the rectangle, i.e. element 2.
  const T& GetWidth() const { return this->Data[2]; }
  const T& Width() const { return this->Data[2]; }

  // Description:
  // Set the height of the rectangle, i.e. element 3.
  void SetHeight(const T& height) { this->Data[3] = height; }

  // Description:
  // Get the height of the rectangle, i.e. element 3.
  const T& GetHeight() const { return this->Data[3]; }
  const T& Height() const { return this->Data[3]; }

};

class vtkRecti : public vtkRect<int>
{
public:
  vtkRecti() {}
  vtkRecti(int x, int y, int width, int height)
    : vtkRect<int>(x, y, width, height) {}
  explicit vtkRecti(const int *init) : vtkRect<int>(init) {}
};

class vtkRectf : public vtkRect<BT_FLOAT>
{
public:
  vtkRectf() {}
  vtkRectf(BT_FLOAT x, BT_FLOAT y, BT_FLOAT width, BT_FLOAT height)
    : vtkRect<BT_FLOAT>(x, y, width, height) {}
  explicit vtkRectf(const BT_FLOAT *init) : vtkRect<BT_FLOAT>(init) {}
};

class vtkRectd : public vtkRect<BT_DOUBLE>
{
public:
  vtkRectd() {}
  vtkRectd(BT_DOUBLE x, BT_DOUBLE y, BT_DOUBLE width, BT_DOUBLE height)
    : vtkRect<BT_DOUBLE>(x, y, width, height) {}
  explicit vtkRectd(const BT_DOUBLE *init) : vtkRect<BT_DOUBLE>(init) {}
};

#endif // __vtkRect_h
