/*=========================================================================

  Program:   Visualization Toolkit
  Module:    vtkParametricEllipsoid.cxx

  Copyright (c) Ken Martin, Will Schroeder, Bill Lorensen 
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/Copyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even 
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR 
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
#include "vtkParametricEllipsoid.h"
#include "vtkObjectFactory.h"
#include "vtkMath.h"
#include <math.h>

vtkStandardNewMacro(vtkParametricEllipsoid);

//----------------------------------------------------------------------------
vtkParametricEllipsoid::vtkParametricEllipsoid() :
  XRadius(1)
  , YRadius(1)
  , ZRadius(1)
{
  // Preset triangulation parameters
  this->MinimumU = 0;
  this->MinimumV = 0;
  this->MaximumU = 2.0 * vtkMath::Pi();
  this->MaximumV = vtkMath::Pi();

  this->JoinU = 1;
  this->JoinV = 0;
  this->TwistU = 0;
  this->TwistV = 0;
  this->ClockwiseOrdering = 1;
  this->DerivativesAvailable = 1;
}

//----------------------------------------------------------------------------
vtkParametricEllipsoid::~vtkParametricEllipsoid()
{
}

//----------------------------------------------------------------------------
void vtkParametricEllipsoid::Evaluate(BT_DOUBLE uvw[3], BT_DOUBLE Pt[3], BT_DOUBLE Duvw[9])
{
  BT_DOUBLE u = uvw[0];
  BT_DOUBLE v = uvw[1];
  BT_DOUBLE *Du = Duvw;
  BT_DOUBLE *Dv = Duvw + 3;

  for ( int i = 0; i < 3; ++i)
    {
    Pt[i] = Du[i] = Dv[i] = 0;
    }

  BT_DOUBLE cu = cos(u);
  BT_DOUBLE su = sin(u);
  BT_DOUBLE cv = cos(v);
  BT_DOUBLE sv = sin(v);

  // The point
  Pt[0] = this->XRadius*sv*cu;
  Pt[1] = this->YRadius*sv*su;
  Pt[2] = this->ZRadius*cv;

  //The derivatives are:
  Du[0] = -this->XRadius*sv*su;
  Du[1] = this->YRadius*sv*cu;
  Du[2] = 0;
  Dv[0] = this->XRadius*cv*cu;
  Dv[1] = this->YRadius*cv*su;
  Dv[2] = -this->ZRadius*sv;

}

//----------------------------------------------------------------------------
BT_DOUBLE vtkParametricEllipsoid::EvaluateScalar(BT_DOUBLE*, BT_DOUBLE*, BT_DOUBLE*)
{
  return 0;
}

//----------------------------------------------------------------------------
void vtkParametricEllipsoid::PrintSelf(ostream& os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os,indent);
  
  os << indent << "X scale factor: " << this->XRadius << "\n";
  os << indent << "Y scale factor: " << this->YRadius << "\n";
  os << indent << "Z scale factor: " << this->ZRadius << "\n";
}

