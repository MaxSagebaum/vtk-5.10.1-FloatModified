/*=========================================================================

  Program:   Visualization Toolkit
  Module:    vtkParametricRandomHills.cxx

  Copyright (c) Ken Martin, Will Schroeder, Bill Lorensen 
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/Copyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even 
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR 
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
#include "vtkParametricRandomHills.h"
#include "vtkObjectFactory.h"
#include "vtkMath.h"
#include "vtkDoubleArray.h"

#include <time.h>

vtkStandardNewMacro(vtkParametricRandomHills);

//----------------------------------------------------------------------------
vtkParametricRandomHills::vtkParametricRandomHills() :
  NumberOfHills(30)
  , HillXVariance(2.5)
  , HillYVariance(2.5)
  , HillAmplitude(2)
  , RandomSeed(1)
  , XVarianceScaleFactor(1.0/3.0)
  , YVarianceScaleFactor(1.0/3.0)
  , AmplitudeScaleFactor(1.0/3.0)
  , AllowRandomGeneration(1)
{
  // Preset triangulation parameters
  this->MinimumU = -10;
  this->MinimumV = -10;
  this->MaximumU = 10;
  this->MaximumV = 10;

  this->JoinU = 0;
  this->JoinV = 0;
  this->TwistU = 0;
  this->TwistV = 0;
  this->ClockwiseOrdering = 1;
  this->DerivativesAvailable = 0;

  this->hillData = vtkDoubleArray::New();

  GenerateTheHills();
}

//----------------------------------------------------------------------------
vtkParametricRandomHills::~vtkParametricRandomHills()
{
  this->hillData->Delete();
}

//----------------------------------------------------------------------------
void vtkParametricRandomHills::InitSeed ( int randomSeed )
{
  if ( randomSeed < 0 )
    {
    randomSeed = performCast<int>(time( NULL ));
    }
  srand( performCast<unsigned int>(randomSeed) );
}

//----------------------------------------------------------------------------
BT_DOUBLE vtkParametricRandomHills::Rand ( void )
{
  return BT_DOUBLE(rand())/BT_DOUBLE(RAND_MAX);
}

//----------------------------------------------------------------------------
void vtkParametricRandomHills::Evaluate(BT_DOUBLE uvw[3], BT_DOUBLE Pt[3], BT_DOUBLE Duvw[9])
{
  BT_DOUBLE u = uvw[0];
  BT_DOUBLE v = uvw[1];
  BT_DOUBLE *Du = Duvw;
  BT_DOUBLE *Dv = Duvw + 3;

  // Zero out the point and derivatives.
  for ( int i = 0; i < 3; ++i )
    Pt[i] = Du[i] = Dv[i] = 0;

  // The point
  // The height of the surface is made up from
  // the contributions from all the Hills.
  Pt[0] = u;
  Pt[1] = this->MaximumV - v; // Texturing is oriented OK if we do this.
  BT_DOUBLE hillTuple[5]; // 0: mX, 1: mY, 2: VarX, 3: VarY, 4: Amplitude
  for ( int j = 0; j < NumberOfHills; ++j )
    {
    this->hillData->GetTuple(j,hillTuple);
    BT_DOUBLE x = (u - hillTuple[0])/hillTuple[2];
    BT_DOUBLE y = (v - hillTuple[1])/hillTuple[3];
    Pt[2] += hillTuple[4] * exp( -(x*x+y*y) / 2.0 );
    }
}

//----------------------------------------------------------------------------
BT_DOUBLE vtkParametricRandomHills::EvaluateScalar(BT_DOUBLE* vtkNotUsed(uv[3]), 
                                                BT_DOUBLE* vtkNotUsed(Pt[3]), 
                                                BT_DOUBLE* vtkNotUsed(Duv[9]))
{
  return 0;
}

void vtkParametricRandomHills::GenerateTheHills( void )
{
  this->hillData->Initialize();
  this->hillData->SetNumberOfComponents(5);
  this->hillData->SetNumberOfTuples(NumberOfHills);

  BT_DOUBLE hillTuple[5]; // 0: mX, 1: mY, 2: VarX, 3: VarY, 4: Amplitude
  // Generate the centers of the Hills, standard deviations and amplitudes.
  if ( AllowRandomGeneration != 0 )
  {
    InitSeed(this->RandomSeed);
    for ( int i = 0; i < this->NumberOfHills; ++ i )
      {
      hillTuple[0] = MinimumU + Rand() * (MaximumU - MinimumU);
      hillTuple[1] = MinimumV + Rand() * (MaximumV - MinimumV);
      hillTuple[2] = this->HillXVariance * Rand() + this->HillXVariance * this->XVarianceScaleFactor;
      hillTuple[3] = this->HillYVariance * Rand() + this->HillYVariance * this->YVarianceScaleFactor;
      hillTuple[4] = this->HillAmplitude * Rand() + this->HillAmplitude * this->AmplitudeScaleFactor;
      this->hillData->SetTuple(i,hillTuple);
      }
  }
  else
    {
    // Here the generation is nonrandom.
    // We put hills in a regular grid over the whole surface.
    BT_DOUBLE randVal = 0.1;
    BT_DOUBLE gridMax = sqrt(performCast<BT_DOUBLE>(this->NumberOfHills));
    int counter = 0;

    BT_DOUBLE midU = (MaximumU - MinimumU)/2.0;
    BT_DOUBLE shiftU = midU / gridMax;
    BT_DOUBLE midV = (MaximumV - MinimumV)/2.0;
    BT_DOUBLE shiftV = midV / gridMax;

    hillTuple[2] = this->HillXVariance * randVal + this->HillXVariance * this->XVarianceScaleFactor;
    hillTuple[3] = this->HillYVariance * randVal + this->HillYVariance * this->YVarianceScaleFactor;
    hillTuple[4] = this->HillAmplitude * randVal * 2.0 + this->HillAmplitude * this->AmplitudeScaleFactor;

    for ( int i = 0; i < performCast<int>(gridMax); ++i )
      {
      hillTuple[0] = MinimumU + shiftU + (i / gridMax) * (MaximumU - MinimumU);
      for ( int j = 0; j < performCast<int>(gridMax); ++j )
        {
        hillTuple[1] = MinimumV + shiftV + (j / gridMax) * (MaximumV - MinimumV);
        this->hillData->SetTuple(counter,hillTuple);
        ++counter;
       }
      }
    // If the number of hills is not a perfect square, set the amplitude contribution 
    // from the rest of the hills to zero.
    hillTuple[4] = 0;
    for ( int k = counter; k < this->NumberOfHills; ++ k )
      {
      hillTuple[0] = MinimumU + midU;
      hillTuple[1] = MinimumV + midV;
      this->hillData->SetTuple(k,hillTuple);
      }
    }

  this->Modified();
}

//----------------------------------------------------------------------------
void vtkParametricRandomHills::PrintSelf(ostream& os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os,indent);

   os << indent << "Hills: " << this->NumberOfHills << "\n";
   os << indent << "Hill variance x-direction: " << this->HillXVariance << "\n";
   os << indent << "Hill variance x-direction scaling factor: " << this->XVarianceScaleFactor << "\n";
   os << indent << "Hill variance y-direction: " << this->HillYVariance << "\n";
   os << indent << "Hill variance y-direction scaling factor: " << this->YVarianceScaleFactor << "\n";
   os << indent << "Hill amplitude (height): " << this->HillAmplitude << "\n";
   os << indent << "Amplitude scaling factor: " << this->AmplitudeScaleFactor << "\n";
   os << indent << "Random number generator seed: " << this->RandomSeed << "\n";
   os << indent << "Allow random generation: " << this->AllowRandomGeneration << "\n";
}
