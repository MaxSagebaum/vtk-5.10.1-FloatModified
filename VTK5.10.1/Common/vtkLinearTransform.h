/*=========================================================================

  Program:   Visualization Toolkit
  Module:    vtkLinearTransform.h

  Copyright (c) Ken Martin, Will Schroeder, Bill Lorensen
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/Copyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkLinearTransform - abstract superclass for linear transformations
// .SECTION Description
// vtkLinearTransform provides a generic interface for linear 
// (affine or 12 degree-of-freedom) geometric transformations. 
// .SECTION see also
// vtkTransform vtkIdentityTransform 


#ifndef __vtkLinearTransform_h
#define __vtkLinearTransform_h

#include "vtkHomogeneousTransform.h"

class VTK_COMMON_EXPORT vtkLinearTransform : public vtkHomogeneousTransform
{
public:

  vtkTypeMacro(vtkLinearTransform,vtkHomogeneousTransform);
  void PrintSelf(ostream& os, vtkIndent indent);

  // Description:
  // Apply the transformation to a normal.
  // You can use the same array to store both the input and output.
  void TransformNormal(const BT_FLOAT in[3], BT_FLOAT out[3]) {
    this->Update(); this->InternalTransformNormal(in,out); };

  // Description:
  // Apply the transformation to a BT_DOUBLE-precision normal.
  // You can use the same array to store both the input and output.
  void TransformNormal(const BT_DOUBLE in[3], BT_DOUBLE out[3]) {
    this->Update(); this->InternalTransformNormal(in,out); };

  // Description:
  // Synonymous with TransformDoubleNormal(x,y,z).
  // Use this if you are programming in python, tcl or Java.
  BT_DOUBLE *TransformNormal(BT_DOUBLE x, BT_DOUBLE y, BT_DOUBLE z) {
    return this->TransformDoubleNormal(x,y,z); }
  BT_DOUBLE *TransformNormal(const BT_DOUBLE normal[3]) {
    return this->TransformDoubleNormal(normal[0],normal[1],normal[2]); };

  // Description:
  // Apply the transformation to an (x,y,z) normal.
  // Use this if you are programming in python, tcl or Java.
  BT_FLOAT *TransformFloatNormal(BT_FLOAT x, BT_FLOAT y, BT_FLOAT z) {
    this->InternalFloatPoint[0] = x;
    this->InternalFloatPoint[1] = y;
    this->InternalFloatPoint[2] = z;
    this->TransformNormal(this->InternalFloatPoint,this->InternalFloatPoint);
    return this->InternalFloatPoint; };
  BT_FLOAT *TransformFloatNormal(const BT_FLOAT normal[3]) {
    return this->TransformFloatNormal(normal[0],normal[1],normal[2]); };

  // Description:
  // Apply the transformation to a BT_DOUBLE-precision (x,y,z) normal.
  // Use this if you are programming in python, tcl or Java.
  BT_DOUBLE *TransformDoubleNormal(BT_DOUBLE x, BT_DOUBLE y, BT_DOUBLE z) {
    this->InternalDoublePoint[0] = x;
    this->InternalDoublePoint[1] = y;
    this->InternalDoublePoint[2] = z;
    this->TransformNormal(this->InternalDoublePoint,this->InternalDoublePoint);
    return this->InternalDoublePoint; };
  BT_DOUBLE *TransformDoubleNormal(const BT_DOUBLE normal[3]) {
    return this->TransformDoubleNormal(normal[0],normal[1],normal[2]); };

  // Description:
  // Synonymous with TransformDoubleVector(x,y,z).
  // Use this if you are programming in python, tcl or Java.
  BT_DOUBLE *TransformVector(BT_DOUBLE x, BT_DOUBLE y, BT_DOUBLE z) {
    return this->TransformDoubleVector(x,y,z); }
  BT_DOUBLE *TransformVector(const BT_DOUBLE normal[3]) {
    return this->TransformDoubleVector(normal[0],normal[1],normal[2]); };

  // Description:
  // Apply the transformation to a vector.
  // You can use the same array to store both the input and output.
  void TransformVector(const BT_FLOAT in[3], BT_FLOAT out[3]) {
    this->Update(); this->InternalTransformVector(in,out); };

  // Description:
  // Apply the transformation to a BT_DOUBLE-precision vector.
  // You can use the same array to store both the input and output.
  void TransformVector(const BT_DOUBLE in[3], BT_DOUBLE out[3]) {
    this->Update(); this->InternalTransformVector(in,out); };

  // Description:
  // Apply the transformation to an (x,y,z) vector.
  // Use this if you are programming in python, tcl or Java.
  BT_FLOAT *TransformFloatVector(BT_FLOAT x, BT_FLOAT y, BT_FLOAT z) {
      this->InternalFloatPoint[0] = x;
      this->InternalFloatPoint[1] = y;
      this->InternalFloatPoint[2] = z;
      this->TransformVector(this->InternalFloatPoint,this->InternalFloatPoint);
      return this->InternalFloatPoint; };
  BT_FLOAT *TransformFloatVector(const BT_FLOAT vec[3]) {
    return this->TransformFloatVector(vec[0],vec[1],vec[2]); };

  // Description:
  // Apply the transformation to a BT_DOUBLE-precision (x,y,z) vector.
  // Use this if you are programming in python, tcl or Java.
  BT_DOUBLE *TransformDoubleVector(BT_DOUBLE x, BT_DOUBLE y, BT_DOUBLE z) {
    this->InternalDoublePoint[0] = x;
    this->InternalDoublePoint[1] = y;
    this->InternalDoublePoint[2] = z;
    this->TransformVector(this->InternalDoublePoint,this->InternalDoublePoint);
    return this->InternalDoublePoint; };
  BT_DOUBLE *TransformDoubleVector(const BT_DOUBLE vec[3]) {
    return this->TransformDoubleVector(vec[0],vec[1],vec[2]); };

  // Description:
  // Apply the transformation to a series of points, and append the
  // results to outPts.  
  void TransformPoints(vtkPoints *inPts, vtkPoints *outPts);

  // Description:
  // Apply the transformation to a series of normals, and append the
  // results to outNms.  
  virtual void TransformNormals(vtkDataArray *inNms, vtkDataArray *outNms);

  // Description:
  // Apply the transformation to a series of vectors, and append the
  // results to outVrs.  
  virtual void TransformVectors(vtkDataArray *inVrs, vtkDataArray *outVrs);

  // Description:
  // Apply the transformation to a combination of points, normals
  // and vectors.  
  void TransformPointsNormalsVectors(vtkPoints *inPts, 
                                     vtkPoints *outPts, 
                                     vtkDataArray *inNms, 
                                     vtkDataArray *outNms,
                                     vtkDataArray *inVrs, 
                                     vtkDataArray *outVrs);

  // Description:
  // Just like GetInverse, but it includes a typecast to 
  // vtkLinearTransform.
  vtkLinearTransform *GetLinearInverse()
    {
      return performCast<vtkLinearTransform *>(this->GetInverse());
    }

  // Description:
  // This will calculate the transformation without calling Update.
  // Meant for use only within other VTK classes.
  void InternalTransformPoint(const BT_FLOAT in[3], BT_FLOAT out[3]);
  void InternalTransformPoint(const BT_DOUBLE in[3], BT_DOUBLE out[3]);

  // Description:
  // This will calculate the transformation without calling Update.
  // Meant for use only within other VTK classes.
  virtual void InternalTransformNormal(const BT_FLOAT in[3], BT_FLOAT out[3]);
  virtual void InternalTransformNormal(const BT_DOUBLE in[3], BT_DOUBLE out[3]);

  // Description:
  // This will calculate the transformation without calling Update.
  // Meant for use only within other VTK classes.
  virtual void InternalTransformVector(const BT_FLOAT in[3], BT_FLOAT out[3]);
  virtual void InternalTransformVector(const BT_DOUBLE in[3], BT_DOUBLE out[3]);

  // Description:
  // This will calculate the transformation as well as its derivative
  // without calling Update.  Meant for use only within other VTK
  // classes.
  void InternalTransformDerivative(const BT_FLOAT in[3], BT_FLOAT out[3],
                                   BT_FLOAT derivative[3][3]);
  void InternalTransformDerivative(const BT_DOUBLE in[3], BT_DOUBLE out[3],
                                   BT_DOUBLE derivative[3][3]);

protected:
  vtkLinearTransform() {};
  ~vtkLinearTransform() {};
private:
  vtkLinearTransform(const vtkLinearTransform&);  // Not implemented.
  void operator=(const vtkLinearTransform&);  // Not implemented.
};

#endif





