/*=========================================================================

  Program:   Visualization Toolkit
  Module:    vtkParametricFigure8Klein.cxx

  Copyright (c) Ken Martin, Will Schroeder, Bill Lorensen 
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/Copyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even 
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR 
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
#include "vtkParametricFigure8Klein.h"
#include "vtkObjectFactory.h"
#include "vtkMath.h"

vtkStandardNewMacro(vtkParametricFigure8Klein);

//----------------------------------------------------------------------------
vtkParametricFigure8Klein::vtkParametricFigure8Klein()
{
  // Preset triangulation parameters
  this->MinimumU = -vtkMath::Pi();
  this->MinimumV = -vtkMath::Pi();
  this->MaximumU = vtkMath::Pi();
  this->MaximumV =  vtkMath::Pi();

  this->JoinU = 1;
  this->JoinV = 1;
  this->TwistU = 1;
  this->TwistV = 0;
  this->ClockwiseOrdering = 1;
  this->DerivativesAvailable = 1;
  this->Radius = 1;
}

//----------------------------------------------------------------------------
vtkParametricFigure8Klein::~vtkParametricFigure8Klein()
{
}

//----------------------------------------------------------------------------
void vtkParametricFigure8Klein::Evaluate(BT_DOUBLE uvw[3], BT_DOUBLE Pt[3], BT_DOUBLE Duvw[9])
{
  BT_DOUBLE u = uvw[0];
  BT_DOUBLE v = uvw[1];
  BT_DOUBLE *Du = Duvw;
  BT_DOUBLE *Dv = Duvw + 3;

  BT_DOUBLE cu = cos(u);
  BT_DOUBLE cu2 = cos(u / 2);
  BT_DOUBLE su = sin(u);
  BT_DOUBLE su2 = sin(u / 2);
  BT_DOUBLE cv = cos(v);
  BT_DOUBLE c2v = cos(2 * v);
  BT_DOUBLE s2v = sin(2 * v);
  BT_DOUBLE sv = sin(v);
  BT_DOUBLE t = this->Radius + sv * cu2 - s2v * su2 / 2;

  // The point
  Pt[0] = cu * t;
  Pt[1] = su * t;
  Pt[2] = su2 * sv + cu2 * s2v / 2;

  //The derivatives are:
  Du[0] = -Pt[1] - cu * ( 2 * sv * su2 + s2v * cu2 ) / 4;
  Du[1] =  Pt[0] - su * ( 2 * sv * su2 + s2v * cu2 ) / 4;
  Du[2] = cu2 * sv / 2 - su2 * s2v / 4;
  Dv[0] = cu * ( cv * cu2 - c2v * su2);
  Dv[1] = su * ( cv * cu2 - c2v * su2);
  Dv[2] = su2 * cv / 2 + cu2 * c2v;
}

//----------------------------------------------------------------------------
BT_DOUBLE vtkParametricFigure8Klein::EvaluateScalar(BT_DOUBLE*, BT_DOUBLE*, BT_DOUBLE*)
{
  return 0;
}

//----------------------------------------------------------------------------
void vtkParametricFigure8Klein::PrintSelf(ostream& os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os,indent);

   os << indent << "Radius: " << this->Radius << "\n";
}

