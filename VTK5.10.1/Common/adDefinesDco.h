#ifndef AD_DEFINES_DCO_H
#define AD_DEFINES_DCO_H

#include <dco.hpp>

#ifdef DCO_LIB
typedef double DCO_BASE_TYPE;
#endif

typedef dco_a1s::typeExt ADoDFloat;

#ifdef DCO_LIB
namespace dco {
#endif
template <> struct Impl_getValue <dco_a1s::typeExt >{
  static inline DCO_BASE_TYPE getValue(const dco_a1s::typeExt &value) {
    return value._value();
  }
};
#ifdef DCO_LIB
}
#endif

template<typename T> static inline double floor(const T &a) {
	return floor(getValue(a));
}

template<typename OT, typename IT> class Impl_performCast {
    public:
        static inline OT performCast(const IT &a) {
            return static_cast<OT>(Impl_getValue<IT>::getValue(a));
        }
        static inline OT performCast(IT &a) {
            return static_cast<OT>(Impl_getValue<IT>::getValue(a));
        }
};

template<typename IT> class Impl_performCast<dco_a1s::type, IT> {
    public:
        static inline dco_a1s::type performCast(const IT &a) {
            dco_a1s::type ret = dco_a1s::type(a);
            return ret;
        }

        static inline dco_a1s::type performCast(IT &a) {
            dco_a1s::type ret = dco_a1s::type(a);
            return ret;
        }
};

template<typename IT> class Impl_performCast<dco_a1s::typeExt, IT> {
    public:
        static inline dco_a1s::typeExt performCast(const IT &a) {
            dco_a1s::type ret = dco_a1s::type(a);
            return dco_a1s::typeExt(ret);
        }

        static inline dco_a1s::typeExt performCast(IT &a) {
            dco_a1s::type ret = dco_a1s::type(a);
            return dco_a1s::typeExt(ret);
        }
};

template<typename OT, typename IT> static inline OT performCast(const IT &a) {
    return Impl_performCast<OT, IT>::performCast(a);
}

template<typename OT, typename IT> static inline OT performCast(IT &a) {
    return Impl_performCast<OT, IT>::performCast(a);
}

static inline dco_a1s::type modf(const dco_a1s::type &x, dco_a1s::type *intpart) {
	double dIntpart;
	double result = modf(getValue(x), &dIntpart);
	*intpart = dIntpart;

	return result;
}


static inline dco_a1s::type fmax(const dco_a1s::type &x, const dco_a1s::type &y) {
	if(x < y) {
		return y;
	} else {
		return x;
	}
}

static inline dco_a1s::type fmin(const dco_a1s::type &x, const dco_a1s::type &y) {
	if(x < y) {
		return x;
	} else {
		return y;
	}
}

inline bool operator!= (dco_a1s::type a, dco_a1s::type b){
  DCO_BASE_TYPE val_a;
  DCO_BASE_TYPE val_b;
  dco_a1s::get(a,val_a);
  dco_a1s::get(b,val_b);
  return !(val_a==val_b);
}

inline bool operator!= (dco_a1s::type a, bool b){
  DCO_BASE_TYPE val_a;
  dco_a1s::get(a,val_a);
  return !(val_a==b);
}

inline bool operator!= (bool a, dco_a1s::type b){
  DCO_BASE_TYPE val_b;
  dco_a1s::get(b,val_b);
  return !(a==val_b);
}

inline bool operator! (dco_a1s::type a){
  DCO_BASE_TYPE val_a;
  dco_a1s::get(a,val_a);
  return (val_a==0.0);
}

inline bool operator&& (dco_a1s::type a, dco_a1s::type b){
  DCO_BASE_TYPE val_a;
  DCO_BASE_TYPE val_b;
  dco_a1s::get(a,val_a);
  dco_a1s::get(b,val_b);
  return (val_a && val_b);
}

inline bool operator&& (bool a, dco_a1s::type b){
  DCO_BASE_TYPE val_b;
  dco_a1s::get(b,val_b);
  return (a && val_b);
}

inline bool operator&& (dco_a1s::type a, bool b){
  DCO_BASE_TYPE val_a;
  dco_a1s::get(a,val_a);
  return (val_a && b);
}

inline bool operator|| (dco_a1s::type a, dco_a1s::type b){
  DCO_BASE_TYPE val_a;
  DCO_BASE_TYPE val_b;
  dco_a1s::get(a,val_a);
  dco_a1s::get(b,val_b);
  return (val_a || val_b);
}

inline bool operator|| (bool a, dco_a1s::type b){
  DCO_BASE_TYPE val_b;
  dco_a1s::get(b,val_b);
  return (a || val_b);
}

inline bool operator|| (dco_a1s::type a, bool b){
  DCO_BASE_TYPE val_a;
  dco_a1s::get(a,val_a);
  return (val_a || b);
}

#endif // AD_DEFINES_H
