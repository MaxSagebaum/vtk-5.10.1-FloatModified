/*=========================================================================

  Program:   Visualization Toolkit
  Module:    vtkParametricRoman.cxx

  Copyright (c) Ken Martin, Will Schroeder, Bill Lorensen 
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/Copyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even 
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR 
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
#include "vtkParametricRoman.h"
#include "vtkObjectFactory.h"
#include "vtkMath.h"

vtkStandardNewMacro(vtkParametricRoman);

//----------------------------------------------------------------------------
vtkParametricRoman::vtkParametricRoman():
  Radius(1)
{
  this->MinimumU = 0;
  this->MinimumV = 0;
  this->MaximumU = vtkMath::Pi();
  this->MaximumV = vtkMath::Pi();

  this->JoinU = 1;
  this->JoinV = 1;
  this->TwistU = 1;
  this->TwistV = 0;
  this->ClockwiseOrdering = 1;
  this->DerivativesAvailable = 1;
}

//----------------------------------------------------------------------------
vtkParametricRoman::~vtkParametricRoman()
{
}

//----------------------------------------------------------------------------
void vtkParametricRoman::Evaluate(BT_DOUBLE uvw[3], BT_DOUBLE Pt[3], BT_DOUBLE Duvw[9])
{
  BT_DOUBLE u = uvw[0];
  BT_DOUBLE v = uvw[1];
  BT_DOUBLE *Du = Duvw;
  BT_DOUBLE *Dv = Duvw + 3;

  BT_DOUBLE cu = cos(u);
  BT_DOUBLE c2u = cos(2.0*u);
  BT_DOUBLE su = sin(u);
  BT_DOUBLE s2u = sin(2.0*u);
  BT_DOUBLE cv = cos(v);
  BT_DOUBLE cv2 = cv*cv;
  BT_DOUBLE c2v = cos(2.0*v);
  BT_DOUBLE s2v = sin(2.0*v);
  BT_DOUBLE sv = sin(v);
  BT_DOUBLE a2 = this->Radius*this->Radius;

  // The point
  Pt[0] = a2*cv2*s2u/2.0;
  Pt[1] = a2*su*s2v/2.0;
  Pt[2] = a2*cu*s2v/2.0;

  //The derivatives are:
  Du[0] = a2*cv2*c2u;
  Du[1] =  a2*cu*s2v/2.0;
  Du[2] = -a2*su*s2v/2.0;
  Dv[0] = -a2*cv*s2u*sv;
  Dv[1] = a2*su*c2v;
  Dv[2] = a2*cu*c2v;
}

//----------------------------------------------------------------------------
BT_DOUBLE vtkParametricRoman::EvaluateScalar(BT_DOUBLE* vtkNotUsed(uv[3]), 
                                          BT_DOUBLE* vtkNotUsed(Pt[3]), 
                                          BT_DOUBLE* vtkNotUsed(Duv[9]))
{
  return 0;
}

//----------------------------------------------------------------------------
void vtkParametricRoman::PrintSelf(ostream& os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os,indent);

   os << indent << "Radius: " << this->Radius << "\n";

}
