/*=========================================================================

  Program:   Visualization Toolkit
  Module:    vtkParametricMobius.cxx

  Copyright (c) Ken Martin, Will Schroeder, Bill Lorensen 
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/Copyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even 
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR 
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
#include "vtkParametricMobius.h"
#include "vtkObjectFactory.h"
#include "vtkMath.h"

vtkStandardNewMacro(vtkParametricMobius);

//----------------------------------------------------------------------------
vtkParametricMobius::vtkParametricMobius()
{
  this->MinimumU = 0;
  this->MinimumV = -1;
  this->MaximumU = 2 * vtkMath::Pi();
  this->MaximumV = 1;

  this->JoinU = 1;
  this->JoinV = 0;
  this->TwistU = 1;
  this->TwistV = 0;
  this->ClockwiseOrdering = 1;
  this->DerivativesAvailable = 1;

  this->Radius = 1;
}

//----------------------------------------------------------------------------
vtkParametricMobius::~vtkParametricMobius()
{
}

//----------------------------------------------------------------------------
void vtkParametricMobius::Evaluate(BT_DOUBLE uvw[3], BT_DOUBLE Pt[3], BT_DOUBLE Duvw[9])
{
  BT_DOUBLE u = uvw[0];
  BT_DOUBLE v = uvw[1];
  BT_DOUBLE *Du = Duvw;
  BT_DOUBLE *Dv = Duvw+3;

  BT_DOUBLE cu = cos(u);
  BT_DOUBLE cu2 = cos( u / 2 );
  BT_DOUBLE su = sin(u);
  BT_DOUBLE su2 = sin( u  / 2 );
  BT_DOUBLE t = this->Radius - v * su2;

  // The point
  Pt[0] = t * su;
  Pt[1] = t * cu;
  Pt[2] = v * cu2;

  //The derivatives are:
  Du[0] = -v*cu2*su/2+Pt[1];
  Du[1] = -v*cu2*cu/2-Pt[0];
  Du[2] = -v*su2/2;
  Dv[0] = -su2*su;
  Dv[1] = -su2*cu;
  Dv[2] = cu2;
}

//----------------------------------------------------------------------------
BT_DOUBLE vtkParametricMobius::EvaluateScalar(BT_DOUBLE *, BT_DOUBLE*, BT_DOUBLE *)
{
  return 0;
}

//----------------------------------------------------------------------------
void vtkParametricMobius::PrintSelf(ostream& os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os,indent);

  os << indent << "Radius: " << this->Radius << "\n";
}
