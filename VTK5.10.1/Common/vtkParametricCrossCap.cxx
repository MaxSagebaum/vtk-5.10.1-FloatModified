/*=========================================================================

  Program:   Visualization Toolkit
  Module:    vtkParametricCrossCap.cxx

  Copyright (c) Ken Martin, Will Schroeder, Bill Lorensen 
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/Copyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even 
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR 
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
#include "vtkParametricCrossCap.h"
#include "vtkObjectFactory.h"
#include "vtkMath.h"

vtkStandardNewMacro(vtkParametricCrossCap);

//----------------------------------------------------------------------------
vtkParametricCrossCap::vtkParametricCrossCap()
{
  // Preset triangulation parameters
  this->MinimumU = 0;
  this->MinimumV = 0;
  this->MaximumU = vtkMath::Pi();
  this->MaximumV = vtkMath::Pi();

  this->JoinU = 1;
  this->JoinV = 1;
  this->TwistU = 1;
  this->TwistV = 1;
  this->ClockwiseOrdering = 1;
  this->DerivativesAvailable = 1;
}

//----------------------------------------------------------------------------
vtkParametricCrossCap::~vtkParametricCrossCap()
{
}

//----------------------------------------------------------------------------
void vtkParametricCrossCap::Evaluate(BT_DOUBLE uvw[3], BT_DOUBLE Pt[3], BT_DOUBLE Duvw[9])
{

  BT_DOUBLE u = uvw[0];
  BT_DOUBLE v = uvw[1];
  BT_DOUBLE *Du = Duvw;
  BT_DOUBLE *Dv = Duvw + 3;

  BT_DOUBLE cu = cos(u);
  BT_DOUBLE su = sin(u);
  BT_DOUBLE cv = cos(v);
  BT_DOUBLE c2v = cos(2 * v);
  BT_DOUBLE sv = sin(v);
  BT_DOUBLE s2v = sin(2 * v);

  // The point
  Pt[0] = cu * s2v;
  Pt[1] = su * s2v;
  Pt[2] = cv * cv - cu * cu * sv * sv;

  //The derivatives are:
  Du[0] = -Pt[1];
  Du[1] = Pt[0];
  Du[2] = 2 * cu * su * sv * sv;
  Dv[0] = 2 * cu * c2v;
  Dv[1] = 2 * su * c2v;
  Dv[2] = -2 * cv * sv * (1 + cu * cu);
}

//----------------------------------------------------------------------------
BT_DOUBLE vtkParametricCrossCap::EvaluateScalar(BT_DOUBLE *, BT_DOUBLE *, BT_DOUBLE *)
{
  return 0;
}

//----------------------------------------------------------------------------
void vtkParametricCrossCap::PrintSelf(ostream& os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os,indent);
}
