#ifndef AD_DEFINES_H
#define AD_DEFINES_H

#ifdef ADTOOL_DOUBLE
#define NORMAL_TYPE
#elif ADTOOL_DCO
#define DCO_A1S_TYPE 1
#elif ADTOOL_ADOLC
#define ADOLC_TYPE 1
#else
#define ADTOOL_DOUBLE
#endif


#ifdef ADTOOL_DOUBLE
# include "adDefinesDouble.h"
#elif ADTOOL_DCO
# include "adDefinesDco.h"
#elif ADTOOL_ADOLC
# include "adDefinesAdolc.h"
#else
# error Please define a mode
#endif

typedef ADoD BT_DOUBLE;
typedef ADoDFloat BT_FLOAT;

#endif // AD_DEFINES_H
