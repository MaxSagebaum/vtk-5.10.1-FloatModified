/*=========================================================================

  Program:   Visualization Toolkit
  Module:    vtkParametricBoy.cxx

  Copyright (c) Ken Martin, Will Schroeder, Bill Lorensen 
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/Copyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even 
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR 
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
#include "vtkParametricBoy.h"
#include "vtkObjectFactory.h"
#include "vtkMath.h"

vtkStandardNewMacro(vtkParametricBoy);

//----------------------------------------------------------------------------
vtkParametricBoy::vtkParametricBoy()
{
  // Preset triangulation parameters
  this->MinimumU = 0;
  this->MinimumV = 0;
  this->MaximumU = vtkMath::Pi();
  this->MaximumV = vtkMath::Pi();

  this->JoinU = 1;
  this->JoinV = 1;
  this->TwistU = 1;
  this->TwistV = 1;
  this->ClockwiseOrdering = 1;
  this->DerivativesAvailable = 1;

  this->ZScale = 0.125;
}

//----------------------------------------------------------------------------
vtkParametricBoy::~vtkParametricBoy()
{
}

//----------------------------------------------------------------------------
void vtkParametricBoy::Evaluate(BT_DOUBLE uvw[3], BT_DOUBLE Pt[3], BT_DOUBLE Duvw[9])
{

  BT_DOUBLE u = uvw[0];
  BT_DOUBLE v = uvw[1];
  BT_DOUBLE *Du = Duvw;
  BT_DOUBLE *Dv = Duvw + 3;

  BT_DOUBLE cu = cos(u);
  BT_DOUBLE su = sin(u);
  BT_DOUBLE sv = sin(v);

  BT_DOUBLE X = cos(u)*sin(v);
  BT_DOUBLE Y = sin(u)*sin(v);
  BT_DOUBLE Z = cos(v);

  BT_DOUBLE X2 = X * X;
  BT_DOUBLE X3 = X2 * X;
  BT_DOUBLE X4 = X3 * X;
  BT_DOUBLE Y2 = Y * Y;
  BT_DOUBLE Y3 = Y2 * Y;
  BT_DOUBLE Y4 = Y3 * Y;
  BT_DOUBLE Z2 = Z * Z;
  BT_DOUBLE Z3 = Z2 * Z;
  BT_DOUBLE Z4 = Z3 * Z;

  // The point
  Pt[0] = 1.0/2.0*(2*X2-Y2-Z2+2.0*Y*Z*(Y2-Z2)+
    Z*X*(X2-Z2)+X*Y*(Y2-X2));
  Pt[1] = sqrt(3.0)/2.0*(Y2-Z2+(Z*X*(Z2-X2)+
    X*Y*(Y2-X2)));
  Pt[2] = this->ZScale*(X+Y+Z)*((X+Y+Z)*(X+Y+Z)*(X+Y+Z)+
    4.0*(Y-X)*(Z-Y)*(X-Z));

  //The derivatives are:
  Du[0] = -1.0/2.0*X4-Z3*X+3.0*Y2*X2-3.0/2.0*Z*X2*Y+3.0*Z*X*Y2-
    3.0*Y*X-1.0/2.0*Y4+1.0/2.0*Z3*Y;
  Dv[0] = (3.0/2.0*Z2*X2+2*Z*X-1.0/2.0*Z4)*cu+
    (-2.0*Z*X3+2*Z*X*Y2+3*Z2*Y2-Z*Y-Z4)*su+
    (-1.0/2.0*X3+3.0/2.0*Z2*X-Y3+3.0*Z2*Y+Z)*sv;
  Du[1] = -1.0/2.0*sqrt(3.0)*X4+3.0*sqrt(3.0)*Y2*X2+
    3.0/2.0*sqrt(3.0)*Z*X2*Y+sqrt(3.0)*Y*X-
    1.0/2.0*sqrt(3.0)*Y4-1.0/2.0*sqrt(3.0)*Z3*Y;
  Dv[1] = (-3.0/2.0*sqrt(3.0)*Z2*X2+1.0/2.0*sqrt(3.0)*Z4)*cu+
    (-2.0*sqrt(3.0)*Z*X3+2.0*sqrt(3.0)*Z*Y2*X+sqrt(3.0)*Z*Y)*su+
    (1.0/2.0*sqrt(3.0)*X3-3.0/2.0*sqrt(3.0)*Z2*X+sqrt(3.0)*Z)*sv;
  Du[2] = X4+3/2*Z*X3+3/2*Z2*X2+X3*Y-3*X2*Y2+
        3*Z*X2*Y-Y3*X-3/2*Z*Y3-3/2*Z2*Y2-Z3*Y;
  Dv[2] = (1/2*Z*X3+3/2*Z3*X+Z4)*cu+(4*Z*X3+3*Z*X2*Y+
        9/2*Z2*X2+9/2*Z2*X*Y+3*Z3*X+1/2*Z*Y3+3*Z2*Y2+
        3/2*Z3*Y)*su+(-3/2*X2*Y-3/2*Z*X2-3/2*X*Y2-
        3*Z*X*Y-3*Z2*X-Y3-3/2*Z*Y2-1/2*Z3)*sv;
}

//----------------------------------------------------------------------------
BT_DOUBLE vtkParametricBoy::EvaluateScalar(BT_DOUBLE *, BT_DOUBLE *, BT_DOUBLE *)
{
  return 0;
}

//----------------------------------------------------------------------------
void vtkParametricBoy::PrintSelf(ostream& os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os,indent);

  os << indent << "ZScale: " << this->ZScale << "\n";

}
