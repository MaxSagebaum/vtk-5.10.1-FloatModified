#ifndef AD_DEFINES_ADOLC_H
#define AD_DEFINES_ADOLC_H

#include <adolc/adolc.h>

#include <adolc/adouble.h>
#include <adolc/interfaces.h>


class adolcExt : public adouble {
public:
	adolcExt() : adouble() {};
	adolcExt(const double &value) : adouble(value) {}
	adolcExt(const adouble &value) : adouble(value) {}
	adolcExt(const adub &value) : adouble(value) {}
};

typedef adolcExt ADoDFloat;
typedef adouble ADoD;

template< typename IN > struct Impl_getValue {
  typedef IN& OUT; // Default implementation has the same output type as input type
  typedef const IN& CONST_OUT; // Default implementation has the same output type as input type

  static inline OUT getValue(IN &value) {
    return value;
  }

  static inline CONST_OUT getValue(const IN &value) {
    return value;
  }

};

template< typename IN > struct Impl_getValue<const IN> {
  typedef const IN& OUT; // Default implementation has the same output type as input type
  typedef const IN& CONST_OUT; // Default implementation has the same output type as input type

  static inline CONST_OUT getValue(const IN &value) {
    return value;
  }

};

template< typename IN, int n> struct Impl_getValue<IN[n]> {
  typedef IN* OUT;
  typedef const IN* CONST_OUT;
  static inline OUT getValue(IN* value) {
    return value;
  }

  static inline CONST_OUT getValue(const IN* value) {
    return value;
  }
};

template< typename IN, int n> struct Impl_getValue<const IN[n]> {
  typedef const IN* OUT;
  typedef const IN* CONST_OUT;

  static inline CONST_OUT getValue(const IN* value) {
    return value;
  }
};

template<> struct Impl_getValue<adouble> {
  typedef double OUT;
  typedef const double CONST_OUT;
  static inline OUT getValue(adouble &value) {
    return value.value();
  }
  static inline CONST_OUT getValue(const adouble &value) {
    return value.value();
  }
};

template<> struct Impl_getValue<adub> {
  typedef double OUT;
  typedef const double CONST_OUT;
  static inline OUT getValue(adub &value) {
    return value.value();
  }
  static inline CONST_OUT getValue(const adub &value) {
    return value.value();
  }
};

template <> struct Impl_getValue <adolcExt >{
  typedef double OUT;
  typedef const double CONST_OUT;

  static inline double getValue(adolcExt &value) {
    return value.value();
  }

  static inline double getValue(const adolcExt &value) {
    return value.value();
  }
};

template <typename IN>
static inline typename Impl_getValue<IN>::CONST_OUT getValue(const IN& x) {
    return Impl_getValue<IN>::getValue(x);
}

template <typename IN>
static inline typename Impl_getValue<IN>::OUT getValue(IN& x) {
    return Impl_getValue<IN>::getValue(x);
}

template<typename T> static inline double floor(const T &a) {
	return floor(getValue(a));
}

template<typename OT, typename IT> class Impl_performCast {
    public:
        static inline OT performCast(const IT &a) {
            return static_cast<OT>(Impl_getValue<IT>::getValue(a));
        }
        static inline OT performCast(IT &a) {
            return static_cast<OT>(Impl_getValue<IT>::getValue(a));
        }
};

template<> class Impl_performCast<adouble, adolcExt> {
    public:
        static inline adouble performCast(const adolcExt &a) {
            return a;
        }
};

template<> class Impl_performCast<adolcExt, adolcExt> {
    public:
        static inline adolcExt performCast(const adolcExt &a) {
            return a;
        }
};

template<> class Impl_performCast<adolcExt, adub> {
    public:
        static inline adolcExt performCast(const adub &a) {
            return adolcExt(a);
        }
};

template<> class Impl_performCast<adouble, adub> {
    public:
        static inline adolcExt performCast(const adub &a) {
            return adouble(a);
        }
};

template<typename OT, typename IT> static inline OT performCast(const IT &a) {
    return Impl_performCast<OT, IT>::performCast(a);
}

template<typename OT, typename IT> static inline OT performCast(IT &a) {
    return Impl_performCast<OT, IT>::performCast(a);
}

static inline adouble modf(const adouble &x, adouble *intpart) {
	double dIntpart;
	double result = modf(getValue(x), &dIntpart);
	*intpart = dIntpart;

	return result;
}

static inline adouble fmax(const adouble &x, const adouble &y) {
	if(x < y) {
		return y;
	} else {
		return x;
	}
}

static inline adouble fmin(const adouble &x, const adouble &y) {
	if(x < y) {
		return x;
	} else {
		return y;
	}
}

static inline adouble abs(const adouble &x) {
    if(x < 0) {
        return -x;
    } else {
        return x;
    }
}

inline bool operator!= (adouble a, adouble b){
  return !(getValue(a)==getValue(b));
}

inline bool operator!= (adouble a, double b){
  return !(getValue(a)==b);
}

inline bool operator!= (double a, adouble b){
  return !(a==getValue(b));
}

inline bool operator! (adouble a){
  return (getValue(a)==0.0);
}

inline bool operator&& (adouble a, adouble b){
  return (getValue(a) && getValue(b));
}

inline bool operator&& (bool a, adouble b){
  return (a && getValue(b));
}

inline bool operator&& (adouble a, bool b){
  return (getValue(b) && b);
}

inline bool operator|| (adouble a, adouble b){
  return (getValue(a) || getValue(b));
}

inline bool operator|| (bool a, adouble b){
  return (a || getValue(b));
}

inline bool operator|| (adouble a, bool b){
  return (getValue(a) || b);
}

template<typename A, typename B> static inline double fmod(const A &x, const B &y) {
    double temp1 = getValue(x);
      double temp2 = getValue(y);

        return fmod(temp1, temp2);
}

template <typename A> static inline double round(const A &x) {
      return round(getValue(x));
}

static inline bool isfinite(const adouble& x) {
      return std::isfinite(x.value());
}

static inline bool isfinite(const adub& x) {
      return std::isfinite(x.value());
}

static inline bool isnan(const adouble& x) {
      return std::isnan(x.value());
}

static inline bool isnan(const adub& x) {
      return std::isnan(x.value());
}

static inline bool isinf(const adouble& x) {
      return std::isinf(x.value());
}

static inline bool isinf(const adub& x) {
      return std::isinf(x.value());
}

#endif // AD_DEFINES_H
