/*=========================================================================

  Program:   Visualization Toolkit
  Module:    vtkMath.h

  Copyright (c) Ken Martin, Will Schroeder, Bill Lorensen
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/Copyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================
  Copyright 2011 Sandia Corporation.
  Under the terms of Contract DE-AC04-94AL85000, there is a non-exclusive
  license for use of this work by or on behalf of the
  U.S. Government. Redistribution and use in source and binary forms, with
  or without modification, are permitted provided that this Notice and any
  statement of authorship are reproduced on all copies.

  Contact: pppebay@sandia.gov,dcthomp@sandia.gov

=========================================================================*/
// .NAME vtkMath - performs common math operations
// .SECTION Description
// vtkMath provides methods to perform common math operations. These
// include providing constants such as Pi; conversion from degrees to
// radians; vector operations such as dot and cross products and vector
// norm; matrix determinant for 2x2 and 3x3 matrices; univariate polynomial
// solvers; and for random number generation (for backward compatibility only).
// .SECTION See Also
// vtkMinimalStandardRandomSequence, vtkBoxMuellerRandomSequence

#ifndef __vtkMath_h
#define __vtkMath_h

#include "vtkObject.h"
#ifndef VTK_LEGACY_REMOVE
# include "vtkPolynomialSolversUnivariate.h" // For backwards compatibility of old solvers
#endif

#include <assert.h> // assert() in inline implementations.

#ifndef DBL_MIN
#  define VTK_DBL_MIN    2.2250738585072014e-308
#else  // DBL_MIN
#  define VTK_DBL_MIN    DBL_MIN
#endif  // DBL_MIN

#ifndef DBL_EPSILON
#  define VTK_DBL_EPSILON    2.2204460492503131e-16
#else  // DBL_EPSILON
#  define VTK_DBL_EPSILON    DBL_EPSILON
#endif  // DBL_EPSILON
#include "vtkMathConfigure.h" // For VTK_HAS_ISINF and VTK_HAS_ISNAN

#include <assert.h> // assert() in inline implementations.

#ifndef VTK_DBL_EPSILON
#  ifndef DBL_EPSILON
#    define VTK_DBL_EPSILON    2.2204460492503131e-16
#  else  // DBL_EPSILON
#    define VTK_DBL_EPSILON    DBL_EPSILON
#  endif  // DBL_EPSILON
#endif  // VTK_DBL_EPSILON

class vtkDataArray;
class vtkPoints;
class vtkMathInternal;
class vtkMinimalStandardRandomSequence;
class vtkBoxMuellerRandomSequence;

class VTK_COMMON_EXPORT vtkMath : public vtkObject
{
public:
  static vtkMath *New();
  vtkTypeMacro(vtkMath,vtkObject);
  void PrintSelf(ostream& os, vtkIndent indent);

  // Description:
  // A mathematical constant. This version is 3.14159265358979f.
  static BT_FLOAT Pi() { return 3.14159265358979f; };

  // Description:
  // A mathematical constant (BT_DOUBLE-precision version). This version
  // is 6.283185307179586.
  static BT_DOUBLE DoubleTwoPi() { return  6.283185307179586; };

  // Description:
  // A mathematical constant (BT_DOUBLE-precision version). This version
  // is 3.1415926535897932384626.
  static BT_DOUBLE DoublePi() { return 3.1415926535897932384626; };

  // Description:
  // Convert degrees into radians
  static BT_FLOAT RadiansFromDegrees( BT_FLOAT degrees);
  static BT_DOUBLE RadiansFromDegrees( BT_DOUBLE degrees);

  // Description:
  // Convert radians into degrees
  static BT_FLOAT DegreesFromRadians( BT_FLOAT radians);
  static BT_DOUBLE DegreesFromRadians( BT_DOUBLE radians);

  // Description:
  // Rounds a BT_FLOAT to the nearest integer.
  static int Round(BT_FLOAT f) {
    return performCast<int>( f + ( f >= 0 ? 0.5 : -0.5 ) ); }
  static int Round(BT_DOUBLE f) {
    return performCast<int>( f + ( f >= 0 ? 0.5 : -0.5 ) ); }

  // Description:
  // Rounds a BT_DOUBLE to the nearest integer not greater than itself.
  // This is faster than floor() but provides undefined output on
  // overflow.
  static int Floor(BT_DOUBLE x);

  // Description:
  // Rounds a BT_DOUBLE to the nearest integer not less than itself.
  // This is faster than ceil() but provides undefined output on
  // overflow.
  static int Ceil(BT_DOUBLE x);

  // Description:
  // Compute N factorial, N! = N*(N-1) * (N-2)...*3*2*1.
  // 0! is taken to be 1.
  static vtkTypeInt64 Factorial( int N );

  // Description:
  // The number of combinations of n objects from a pool of m objects (m>n).
  // This is commonly known as "m choose n" and sometimes denoted \f$_mC_n\f$
  // or \f$\left(\begin{array}{c}m \\ n\end{array}\right)\f$.
  static vtkTypeInt64 Binomial( int m, int n );

  // Description:
  // Start iterating over "m choose n" objects.
  // This function returns an array of n integers, each from 0 to m-1.
  // These integers represent the n items chosen from the set [0,m[.
  //
  // You are responsible for calling vtkMath::FreeCombination() once the iterator is no longer needed.
  //
  // Warning: this gets large very quickly, especially when n nears m/2!
  // (Hint: think of Pascal's triangle.)
  static int* BeginCombination( int m, int n );

  // Description:
  // Given \a m, \a n, and a valid \a combination of \a n integers in
  // the range [0,m[, this function alters the integers into the next
  // combination in a sequence of all combinations of \a n items from
  // a pool of \a m.
  //
  // If the \a combination is the last item in the sequence on input,
  // then \a combination is unaltered and 0 is returned.
  // Otherwise, 1 is returned and \a combination is updated.
  static int NextCombination( int m, int n, int* combination );

  // Description:
  // Free the "iterator" array created by vtkMath::BeginCombination.
  static void FreeCombination( int* combination);

  // Description:
  // Initialize seed value. NOTE: Random() has the bad property that
  // the first random number returned after RandomSeed() is called
  // is proportional to the seed value! To help solve this, call
  // RandomSeed() a few times inside seed. This doesn't ruin the
  // repeatability of Random().
  //
  // DON'T USE Random(), RandomSeed(), GetSeed(), Gaussian()
  // THIS IS STATIC SO THIS IS PRONE TO ERRORS (SPECIALLY FOR REGRESSION TESTS)
  // THIS IS HERE FOR BACKWARD COMPATIBILITY ONLY.
  // Instead, for a sequence of random numbers with a uniform distribution
  // create a vtkMinimalStandardRandomSequence object.
  // For a sequence of random numbers with a gaussian/normal distribution
  // create a vtkBoxMuellerRandomSequence object.
  static void RandomSeed(int s);

  // Description:
  // Return the current seed used by the random number generator.
  //
  // DON'T USE Random(), RandomSeed(), GetSeed(), Gaussian()
  // THIS IS STATIC SO THIS IS PRONE TO ERRORS (SPECIALLY FOR REGRESSION TESTS)
  // THIS IS HERE FOR BACKWARD COMPATIBILITY ONLY.
  // Instead, for a sequence of random numbers with a uniform distribution
  // create a vtkMinimalStandardRandomSequence object.
  // For a sequence of random numbers with a gaussian/normal distribution
  // create a vtkBoxMuellerRandomSequence object.
  static int GetSeed();

  // Description:
  // Generate pseudo-random numbers distributed according to the uniform
  // distribution between 0.0 and 1.0.
  // This is used to provide portability across different systems.
  //
  // DON'T USE Random(), RandomSeed(), GetSeed(), Gaussian()
  // THIS IS STATIC SO THIS IS PRONE TO ERRORS (SPECIALLY FOR REGRESSION TESTS)
  // THIS IS HERE FOR BACKWARD COMPATIBILITY ONLY.
  // Instead, for a sequence of random numbers with a uniform distribution
  // create a vtkMinimalStandardRandomSequence object.
  // For a sequence of random numbers with a gaussian/normal distribution
  // create a vtkBoxMuellerRandomSequence object.
  static BT_DOUBLE Random();

  // Description:
  // Generate  pseudo-random numbers distributed according to the uniform
  // distribution between \a min and \a max.
  //
  // DON'T USE Random(), RandomSeed(), GetSeed(), Gaussian()
  // THIS IS STATIC SO THIS IS PRONE TO ERRORS (SPECIALLY FOR REGRESSION TESTS)
  // THIS IS HERE FOR BACKWARD COMPATIBILITY ONLY.
  // Instead, for a sequence of random numbers with a uniform distribution
  // create a vtkMinimalStandardRandomSequence object.
  // For a sequence of random numbers with a gaussian/normal distribution
  // create a vtkBoxMuellerRandomSequence object.
  static BT_DOUBLE Random( BT_DOUBLE min, BT_DOUBLE max );

  // Description:
  // Generate pseudo-random numbers distributed according to the standard
  // normal distribution.
  //
  // DON'T USE Random(), RandomSeed(), GetSeed(), Gaussian()
  // THIS IS STATIC SO THIS IS PRONE TO ERRORS (SPECIALLY FOR REGRESSION TESTS)
  // THIS IS HERE FOR BACKWARD COMPATIBILITY ONLY.
  // Instead, for a sequence of random numbers with a uniform distribution
  // create a vtkMinimalStandardRandomSequence object.
  // For a sequence of random numbers with a gaussian/normal distribution
  // create a vtkBoxMuellerRandomSequence object.
  static BT_DOUBLE Gaussian();

  // Description:
  // Generate  pseudo-random numbers distributed according to the Gaussian
  // distribution with mean \a mean and standard deviation \a std.
  //
  // DON'T USE Random(), RandomSeed(), GetSeed(), Gaussian()
  // THIS IS STATIC SO THIS IS PRONE TO ERRORS (SPECIALLY FOR REGRESSION TESTS)
  // THIS IS HERE FOR BACKWARD COMPATIBILITY ONLY.
  // Instead, for a sequence of random numbers with a uniform distribution
  // create a vtkMinimalStandardRandomSequence object.
  // For a sequence of random numbers with a gaussian/normal distribution
  // create a vtkBoxMuellerRandomSequence object.
  static BT_DOUBLE Gaussian( BT_DOUBLE mean, BT_DOUBLE std );

  // Description:
  // Addition of two 3-vectors (BT_FLOAT version). Result is stored in c.
  static void Add(const BT_FLOAT a[3], const BT_FLOAT b[3], BT_FLOAT c[3]) {
    for (int i = 0; i < 3; ++i)
      c[i] = a[i] + b[i];
  }

  // Description:
  // Addition of two 3-vectors (BT_DOUBLE version). Result is stored in c.
  static void Add(const BT_DOUBLE a[3], const BT_DOUBLE b[3], BT_DOUBLE c[3]) {
    for (int i = 0; i < 3; ++i)
      c[i] = a[i] + b[i];
  }

  // Description:
  // Subtraction of two 3-vectors (BT_FLOAT version). Result is stored in c according to c = a - b.
  static void Subtract(const BT_FLOAT a[3], const BT_FLOAT b[3], BT_FLOAT c[3]) {
    for (int i = 0; i < 3; ++i)
      c[i] = a[i] - b[i];
  }

  // Description:
  // Subtraction of two 3-vectors (BT_DOUBLE version). Result is stored in c according to c = a - b.
  static void Subtract(const BT_DOUBLE a[3], const BT_DOUBLE b[3], BT_DOUBLE c[3]) {
    for (int i = 0; i < 3; ++i)
      c[i] = a[i] - b[i];
  }

  // Description:
  // Multiplies a 3-vector by a scalar (BT_FLOAT version).
  // This modifies the input 3-vector.
  static void MultiplyScalar(BT_FLOAT a[3], BT_FLOAT s) {
    for (int i = 0; i < 3; ++i)
      a[i] *= s;
  }

  // Description:
  // Multiplies a 2-vector by a scalar (BT_FLOAT version).
  // This modifies the input 2-vector.
  static void MultiplyScalar2D(BT_FLOAT a[2], BT_FLOAT s) {
    for (int i = 0; i < 2; ++i)
      a[i] *= s;
  }

  // Description:
  // Multiplies a 3-vector by a scalar (BT_DOUBLE version).
  // This modifies the input 3-vector.
  static void MultiplyScalar(BT_DOUBLE a[3], BT_DOUBLE s) {
    for (int i = 0; i < 3; ++i)
      a[i] *= s;
  }

  // Description:
  // Multiplies a 2-vector by a scalar (BT_DOUBLE version).
  // This modifies the input 2-vector.
  static void MultiplyScalar2D(BT_DOUBLE a[2], BT_DOUBLE s) {
    for (int i = 0; i < 2; ++i)
      a[i] *= s;
  }

  // Description:
  // Dot product of two 3-vectors (BT_FLOAT version).
  static BT_FLOAT Dot(const BT_FLOAT x[3], const BT_FLOAT y[3]) {
    return ( x[0] * y[0] + x[1] * y[1] + x[2] * y[2] );};

  // Description:
  // Dot product of two 3-vectors (BT_DOUBLE-precision version).
  static BT_DOUBLE Dot(const BT_DOUBLE x[3], const BT_DOUBLE y[3]) {
    return ( x[0] * y[0] + x[1] * y[1] + x[2] * y[2] );};

  // Description:
  // Outer product of two 3-vectors (BT_FLOAT version).
  static void Outer(const BT_FLOAT x[3], const BT_FLOAT y[3], BT_FLOAT A[3][3]) {
    for (int i=0; i < 3; i++)
      for (int j=0; j < 3; j++)
        A[i][j] = x[i] * y[j];
  }
  // Description:
  // Outer product of two 3-vectors (BT_DOUBLE-precision version).
  static void Outer(const BT_DOUBLE x[3], const BT_DOUBLE y[3], BT_DOUBLE A[3][3]) {
    for (int i=0; i < 3; i++)
      for (int j=0; j < 3; j++)
        A[i][j] = x[i] * y[j];
  }

  // Description:
  // Cross product of two 3-vectors. Result (a x b) is stored in z.
  static void Cross(const BT_FLOAT x[3], const BT_FLOAT y[3], BT_FLOAT z[3]);

  // Description:
  // Cross product of two 3-vectors. Result (a x b) is stored in z. (BT_DOUBLE-precision
  // version)
  static void Cross(const BT_DOUBLE x[3], const BT_DOUBLE y[3], BT_DOUBLE z[3]);

  // Description:
  // Compute the norm of n-vector. x is the vector, n is its length.
  static BT_FLOAT Norm(const BT_FLOAT* x, int n);
  static BT_DOUBLE Norm(const BT_DOUBLE* x, int n);

  // Description:
  // Compute the norm of 3-vector.
  static BT_FLOAT Norm(const BT_FLOAT x[3]) {
    return performCast<BT_FLOAT> (sqrt( x[0] * x[0] + x[1] * x[1] + x[2] * x[2] ) );};

  // Description:
  // Compute the norm of 3-vector (BT_DOUBLE-precision version).
  static BT_DOUBLE Norm(const BT_DOUBLE x[3]) {
    return sqrt( x[0] * x[0] + x[1] * x[1] + x[2] * x[2] );};

  // Description:
  // Normalize (in place) a 3-vector. Returns norm of vector.
  static BT_FLOAT Normalize(BT_FLOAT x[3]);

  // Description:
  // Normalize (in place) a 3-vector. Returns norm of vector
  // (BT_DOUBLE-precision version).
  static BT_DOUBLE Normalize(BT_DOUBLE x[3]);

  // Description:
  // Given a unit vector x, find two unit vectors y and z such that
  // x cross y = z (i.e. the vectors are perpendicular to each other).
  // There is an infinite number of such vectors, specify an angle theta
  // to choose one set.  If you want only one perpendicular vector,
  // specify NULL for z.
  static void Perpendiculars(const BT_DOUBLE x[3], BT_DOUBLE y[3], BT_DOUBLE z[3],
                             BT_DOUBLE theta);
  static void Perpendiculars(const BT_FLOAT x[3], BT_FLOAT y[3], BT_FLOAT z[3],
                             BT_DOUBLE theta);

  // Description:
  // Compute the projection of vector a on vector b and return it in projection[3].
  // If b is a zero vector, the function returns false and 'projection' is invalid.
  // Otherwise, it returns true.
  static bool ProjectVector(const BT_FLOAT a[3], const BT_FLOAT b[3], BT_FLOAT projection[3]);
  static bool ProjectVector(const BT_DOUBLE a[3], const BT_DOUBLE b[3], BT_DOUBLE projection[3]);

  // Description:
  // Compute the projection of 2D vector 'a' on 2D vector 'b' and returns the result
  // in projection[2].
  // If b is a zero vector, the function returns false and 'projection' is invalid.
  // Otherwise, it returns true.
  static bool ProjectVector2D(const BT_FLOAT a[2], const BT_FLOAT b[2], BT_FLOAT projection[2]);
  static bool ProjectVector2D(const BT_DOUBLE a[2], const BT_DOUBLE b[2], BT_DOUBLE projection[2]);

  // Description:
  // Compute distance squared between two points x and y.
  static BT_FLOAT Distance2BetweenPoints(const BT_FLOAT x[3], const BT_FLOAT y[3]);

  // Description:
  // Compute distance squared between two points x and y(BT_DOUBLE precision version).
  static BT_DOUBLE Distance2BetweenPoints(const BT_DOUBLE x[3], const BT_DOUBLE y[3]);

  // Description:
  // Compute the amplitude of a Gaussian function with mean=0 and specified variance.
  // That is, 1./(sqrt(2 Pi * variance)) * exp(-distanceFromMean^2/(2.*variance)).
  static BT_DOUBLE GaussianAmplitude(const BT_DOUBLE variance, const BT_DOUBLE distanceFromMean);

  // Description:
  // Compute the amplitude of a Gaussian function with specified mean and variance.
  // That is, 1./(sqrt(2 Pi * variance)) * exp(-(position - mean)^2/(2.*variance)).
  static BT_DOUBLE GaussianAmplitude(const BT_DOUBLE mean, const BT_DOUBLE variance, const BT_DOUBLE position);

  // Description:
  // Compute the amplitude of an unnormalized Gaussian function with mean=0 and specified variance.
  // That is, exp(-distanceFromMean^2/(2.*variance)). When distanceFromMean = 0, this function
  // returns 1.
  static BT_DOUBLE GaussianWeight(const BT_DOUBLE variance, const BT_DOUBLE distanceFromMean);

  // Description:
  // Compute the amplitude of an unnormalized Gaussian function with specified mean and variance.
  // That is, exp(-(position - mean)^2/(2.*variance)). When the distance from 'position' to 'mean'
  // is 0, this function returns 1.
  static BT_DOUBLE GaussianWeight(const BT_DOUBLE mean, const BT_DOUBLE variance, const BT_DOUBLE position);

  // Description:
  // Dot product of two 2-vectors.
  static BT_FLOAT Dot2D(const BT_FLOAT x[2], const BT_FLOAT y[2]) {
    return ( x[0] * y[0] + x[1] * y[1] );};

  // Description:
  // Dot product of two 2-vectors. (BT_DOUBLE-precision version).
  static BT_DOUBLE Dot2D(const BT_DOUBLE x[2], const BT_DOUBLE y[2]) {
    return ( x[0] * y[0] + x[1] * y[1] );};

  // Description:
  // Outer product of two 2-vectors (BT_FLOAT version).
  static void Outer2D(const BT_FLOAT x[2], const BT_FLOAT y[2], BT_FLOAT A[2][2])
    {
    for (int i=0; i < 2; i++)
      {
      for (int j=0; j < 2; j++)
        {
        A[i][j] = x[i] * y[j];
        }
      }
    }
  // Description:
  // Outer product of two 2-vectors (BT_FLOAT version).
  static void Outer2D(const BT_DOUBLE x[2], const BT_DOUBLE y[2], BT_DOUBLE A[2][2])
    {
    for (int i=0; i < 2; i++)
      {
      for (int j=0; j < 2; j++)
        {
        A[i][j] = x[i] * y[j];
        }
      }
    }

  // Description:
  // Compute the norm of a 2-vector.
  static BT_FLOAT Norm2D(const BT_FLOAT x[2]) {
    return performCast<BT_FLOAT> (sqrt( x[0] * x[0] + x[1] * x[1] ) );};

  // Description:
  // Compute the norm of a 2-vector.
  // (BT_DOUBLE-precision version).
  static BT_DOUBLE Norm2D(const BT_DOUBLE x[2]) {
    return sqrt( x[0] * x[0] + x[1] * x[1] );};

  // Description:
  // Normalize (in place) a 2-vector. Returns norm of vector.
  static BT_FLOAT Normalize2D(BT_FLOAT x[2]);

  // Description:
  // Normalize (in place) a 2-vector. Returns norm of vector.
  // (BT_DOUBLE-precision version).
  static BT_DOUBLE Normalize2D(BT_DOUBLE x[2]);

  // Description:
  // Compute determinant of 2x2 matrix. Two columns of matrix are input.
  static BT_FLOAT Determinant2x2(const BT_FLOAT c1[2], const BT_FLOAT c2[2]) {
    return (c1[0] * c2[1] - c2[0] * c1[1] );};

  // Description:
  // Calculate the determinant of a 2x2 matrix: | a b | | c d |
  static BT_DOUBLE Determinant2x2(BT_DOUBLE a, BT_DOUBLE b, BT_DOUBLE c, BT_DOUBLE d) {
    return (a * d - b * c);};
  static BT_DOUBLE Determinant2x2(const BT_DOUBLE c1[2], const BT_DOUBLE c2[2]) {
    return (c1[0] * c2[1] - c2[0] * c1[1] );};

  // Description:
  // LU Factorization of a 3x3 matrix.
  static void LUFactor3x3(BT_FLOAT A[3][3], int index[3]);
  static void LUFactor3x3(BT_DOUBLE A[3][3], int index[3]);

  // Description:
  // LU back substitution for a 3x3 matrix.
  static void LUSolve3x3(const BT_FLOAT A[3][3], const int index[3],
                         BT_FLOAT x[3]);
  static void LUSolve3x3(const BT_DOUBLE A[3][3], const int index[3],
                         BT_DOUBLE x[3]);

  // Description:
  // Solve Ay = x for y and place the result in y.  The matrix A is
  // destroyed in the process.
  static void LinearSolve3x3(const BT_FLOAT A[3][3], const BT_FLOAT x[3],
                             BT_FLOAT y[3]);
  static void LinearSolve3x3(const BT_DOUBLE A[3][3], const BT_DOUBLE x[3],
                             BT_DOUBLE y[3]);

  // Description:
  // Multiply a vector by a 3x3 matrix.  The result is placed in out.
  static void Multiply3x3(const BT_FLOAT A[3][3], const BT_FLOAT in[3],
                          BT_FLOAT out[3]);
  static void Multiply3x3(const BT_DOUBLE A[3][3], const BT_DOUBLE in[3],
                          BT_DOUBLE out[3]);

  // Description:
  // Multiply one 3x3 matrix by another according to C = AB.
  static void Multiply3x3(const BT_FLOAT A[3][3], const BT_FLOAT B[3][3],
                          BT_FLOAT C[3][3]);
  static void Multiply3x3(const BT_DOUBLE A[3][3], const BT_DOUBLE B[3][3],
                          BT_DOUBLE C[3][3]);

  // Description:
  // General matrix multiplication.  You must allocate output storage.
  // colA == rowB
  // and matrix C is rowA x colB
  static void MultiplyMatrix(const BT_DOUBLE **A, const BT_DOUBLE **B,
                             unsigned int rowA, unsigned int colA,
                             unsigned int rowB, unsigned int colB,
                             BT_DOUBLE **C);

  // Description:
  // Transpose a 3x3 matrix. The input matrix is A. The output
  // is stored in AT.
  static void Transpose3x3(const BT_FLOAT A[3][3], BT_FLOAT AT[3][3]);
  static void Transpose3x3(const BT_DOUBLE A[3][3], BT_DOUBLE AT[3][3]);

  // Description:
  // Invert a 3x3 matrix. The input matrix is A. The output is
  // stored in AI.
  static void Invert3x3(const BT_FLOAT A[3][3], BT_FLOAT AI[3][3]);
  static void Invert3x3(const BT_DOUBLE A[3][3], BT_DOUBLE AI[3][3]);

  // Description:
  // Set A to the identity matrix.
  static void Identity3x3(BT_FLOAT A[3][3]);
  static void Identity3x3(BT_DOUBLE A[3][3]);

  // Description:
  // Return the determinant of a 3x3 matrix.
  static BT_DOUBLE Determinant3x3(BT_FLOAT A[3][3]);
  static BT_DOUBLE Determinant3x3(BT_DOUBLE A[3][3]);

  // Description:
  // Compute determinant of 3x3 matrix. Three columns of matrix are input.
  static BT_FLOAT Determinant3x3(const BT_FLOAT c1[3],
                              const BT_FLOAT c2[3],
                              const BT_FLOAT c3[3]);

  // Description:
  // Compute determinant of 3x3 matrix. Three columns of matrix are input.
  static BT_DOUBLE Determinant3x3(const BT_DOUBLE c1[3],
                               const BT_DOUBLE c2[3],
                               const BT_DOUBLE c3[3]);

  // Description:
  // Calculate the determinant of a 3x3 matrix in the form:
  //     | a1,  b1,  c1 |
  //     | a2,  b2,  c2 |
  //     | a3,  b3,  c3 |
  static BT_DOUBLE Determinant3x3(BT_DOUBLE a1, BT_DOUBLE a2, BT_DOUBLE a3,
                               BT_DOUBLE b1, BT_DOUBLE b2, BT_DOUBLE b3,
                               BT_DOUBLE c1, BT_DOUBLE c2, BT_DOUBLE c3);

  // Description:
  // Convert a quaternion to a 3x3 rotation matrix.  The quaternion
  // does not have to be normalized beforehand.
  static void QuaternionToMatrix3x3(const BT_FLOAT quat[4], BT_FLOAT A[3][3]);
  static void QuaternionToMatrix3x3(const BT_DOUBLE quat[4], BT_DOUBLE A[3][3]);

  // Description:
  // Convert a 3x3 matrix into a quaternion.  This will provide the
  // best possible answer even if the matrix is not a pure rotation matrix.
  // The method used is that of B.K.P. Horn.
  static void Matrix3x3ToQuaternion(const BT_FLOAT A[3][3], BT_FLOAT quat[4]);
  static void Matrix3x3ToQuaternion(const BT_DOUBLE A[3][3], BT_DOUBLE quat[4]);

  // Description:
  // Multiply two quaternions. This is used to concatenate rotations
  static void MultiplyQuaternion( const BT_FLOAT q1[4], const BT_FLOAT q2[4],  BT_FLOAT q[4] );
  static void MultiplyQuaternion( const BT_DOUBLE q1[4], const BT_DOUBLE q2[4],  BT_DOUBLE q[4] );

  // Description:
  // Orthogonalize a 3x3 matrix and put the result in B.  If matrix A
  // has a negative determinant, then B will be a rotation plus a flip
  // i.e. it will have a determinant of -1.
  static void Orthogonalize3x3(const BT_FLOAT A[3][3], BT_FLOAT B[3][3]);
  static void Orthogonalize3x3(const BT_DOUBLE A[3][3], BT_DOUBLE B[3][3]);

  // Description:
  // Diagonalize a symmetric 3x3 matrix and return the eigenvalues in
  // w and the eigenvectors in the columns of V.  The matrix V will
  // have a positive determinant, and the three eigenvectors will be
  // aligned as closely as possible with the x, y, and z axes.
  static void Diagonalize3x3(const BT_FLOAT A[3][3], BT_FLOAT w[3], BT_FLOAT V[3][3]);
  static void Diagonalize3x3(const BT_DOUBLE A[3][3],BT_DOUBLE w[3],BT_DOUBLE V[3][3]);

  // Description:
  // Perform singular value decomposition on a 3x3 matrix.  This is not
  // done using a conventional SVD algorithm, instead it is done using
  // Orthogonalize3x3 and Diagonalize3x3.  Both output matrices U and VT
  // will have positive determinants, and the w values will be arranged
  // such that the three rows of VT are aligned as closely as possible
  // with the x, y, and z axes respectively.  If the determinant of A is
  // negative, then the three w values will be negative.
  static void SingularValueDecomposition3x3(const BT_FLOAT A[3][3],
                                            BT_FLOAT U[3][3], BT_FLOAT w[3],
                                            BT_FLOAT VT[3][3]);
  static void SingularValueDecomposition3x3(const BT_DOUBLE A[3][3],
                                            BT_DOUBLE U[3][3], BT_DOUBLE w[3],
                                            BT_DOUBLE VT[3][3]);

  // Description:
  // Solve linear equations Ax = b using Crout's method. Input is square
  // matrix A and load vector x. Solution x is written over load vector. The
  // dimension of the matrix is specified in size. If error is found, method
  // returns a 0.
  static int SolveLinearSystem(BT_DOUBLE **A, BT_DOUBLE *x, int size);

  // Description:
  // Invert input square matrix A into matrix AI.
  // Note that A is modified during
  // the inversion. The size variable is the dimension of the matrix. Returns 0
  // if inverse not computed.
  static int InvertMatrix(BT_DOUBLE **A, BT_DOUBLE **AI, int size);

  // Description:
  // Thread safe version of InvertMatrix method.
  // Working memory arrays tmp1SIze and tmp2Size
  // of length size must be passed in.
  static int InvertMatrix(BT_DOUBLE **A, BT_DOUBLE **AI, int size,
                          int *tmp1Size, BT_DOUBLE *tmp2Size);

  // Description:
  // Factor linear equations Ax = b using LU decomposition A = LU where L is
  // lower triangular matrix and U is upper triangular matrix. Input is
  // square matrix A, integer array of pivot indices index[0->n-1], and size
  // of square matrix n. Output factorization LU is in matrix A. If error is
  // found, method returns 0.
  static int LUFactorLinearSystem(BT_DOUBLE **A, int *index, int size);

  // Description:
  // Thread safe version of LUFactorLinearSystem method.
  // Working memory array tmpSize of length size
  // must be passed in.
  static int LUFactorLinearSystem(BT_DOUBLE **A, int *index, int size,
                                  BT_DOUBLE *tmpSize);

  // Description:
  // Solve linear equations Ax = b using LU decomposition A = LU where L is
  // lower triangular matrix and U is upper triangular matrix. Input is
  // factored matrix A=LU, integer array of pivot indices index[0->n-1],
  // load vector x[0->n-1], and size of square matrix n. Note that A=LU and
  // index[] are generated from method LUFactorLinearSystem). Also, solution
  // vector is written directly over input load vector.
  static void LUSolveLinearSystem(BT_DOUBLE **A, int *index,
                                  BT_DOUBLE *x, int size);

  // Description:
  // Estimate the condition number of a LU factored matrix. Used to judge the
  // accuracy of the solution. The matrix A must have been previously factored
  // using the method LUFactorLinearSystem. The condition number is the ratio
  // of the infinity matrix norm (i.e., maximum value of matrix component)
  // divided by the minimum diagonal value. (This works for triangular matrices
  // only: see Conte and de Boor, Elementary Numerical Analysis.)
  static BT_DOUBLE EstimateMatrixCondition(BT_DOUBLE **A, int size);

  // Description:
  // Jacobi iteration for the solution of eigenvectors/eigenvalues of a 3x3
  // real symmetric matrix. Square 3x3 matrix a; output eigenvalues in w;
  // and output eigenvectors in v. Resulting eigenvalues/vectors are sorted
  // in decreasing order; eigenvectors are normalized.
  static int Jacobi(BT_FLOAT **a, BT_FLOAT *w, BT_FLOAT **v);
  static int Jacobi(BT_DOUBLE **a, BT_DOUBLE *w, BT_DOUBLE **v);

  // Description:
  // JacobiN iteration for the solution of eigenvectors/eigenvalues of a nxn
  // real symmetric matrix. Square nxn matrix a; size of matrix in n; output
  // eigenvalues in w; and output eigenvectors in v. Resulting
  // eigenvalues/vectors are sorted in decreasing order; eigenvectors are
  // normalized.  w and v need to be allocated previously
  static int JacobiN(BT_FLOAT **a, int n, BT_FLOAT *w, BT_FLOAT **v);
  static int JacobiN(BT_DOUBLE **a, int n, BT_DOUBLE *w, BT_DOUBLE **v);

  // Description:
  // Solves a cubic equation c0*t^3 + c1*t^2 + c2*t + c3 = 0 when c0, c1, c2,
  // and c3 are REAL.  Solution is motivated by Numerical Recipes In C 2nd
  // Ed.  Return array contains number of (real) roots (counting multiple
  // roots as one) followed by roots themselves. The value in roots[4] is a
  // integer giving further information about the roots (see return codes for
  // int SolveCubic() ).
  // @deprecated Replaced by vtkPolynomialSolversUnivariate::SolveCubic() as of
  // VTK 5.8.
  VTK_LEGACY(static BT_DOUBLE* SolveCubic(BT_DOUBLE c0, BT_DOUBLE c1, BT_DOUBLE c2, BT_DOUBLE c3));

  // Description:
  // Solves a quadratic equation c1*t^2 + c2*t + c3 = 0 when c1, c2, and c3
  // are REAL.  Solution is motivated by Numerical Recipes In C 2nd Ed.
  // Return array contains number of (real) roots (counting multiple roots as
  // one) followed by roots themselves. Note that roots[3] contains a return
  // code further describing solution - see documentation for SolveCubic()
  // for meaning of return codes.
  // @deprecated Replaced by vtkPolynomialSolversUnivariate::SolveQuadratic() as
  // of VTK 5.8.
  VTK_LEGACY(static BT_DOUBLE* SolveQuadratic(BT_DOUBLE c0, BT_DOUBLE c1, BT_DOUBLE c2));

  // Description:
  // Solves a linear equation c2*t  + c3 = 0 when c2 and c3 are REAL.
  // Solution is motivated by Numerical Recipes In C 2nd Ed.
  // Return array contains number of roots followed by roots themselves.
  // @deprecated Replaced by vtkPolynomialSolversUnivariate::SolveLinear() as of
  // VTK 5.8.
  VTK_LEGACY(static BT_DOUBLE* SolveLinear(BT_DOUBLE c0, BT_DOUBLE c1));

  // Description:
  // Solves a cubic equation when c0, c1, c2, And c3 Are REAL.  Solution
  // is motivated by Numerical Recipes In C 2nd Ed.  Roots and number of
  // real roots are stored in user provided variables r1, r2, r3, and
  // num_roots. Note that the function can return the following integer
  // values describing the roots: (0)-no solution; (-1)-infinite number
  // of solutions; (1)-one distinct real root of multiplicity 3 (stored
  // in r1); (2)-two distinct real roots, one of multiplicity 2 (stored
  // in r1 & r2); (3)-three distinct real roots; (-2)-quadratic equation
  // with complex conjugate solution (real part of root returned in r1,
  // imaginary in r2); (-3)-one real root and a complex conjugate pair
  // (real root in r1 and real part of pair in r2 and imaginary in r3).
  // @deprecated Replaced by vtkPolynomialSolversUnivariate::SolveCubic() as of
  // VTK 5.8.
  VTK_LEGACY(static int SolveCubic(BT_DOUBLE c0, BT_DOUBLE c1, BT_DOUBLE c2, BT_DOUBLE c3,
                        BT_DOUBLE *r1, BT_DOUBLE *r2, BT_DOUBLE *r3, int *num_roots));

  // Description:
  // Solves a quadratic equation c1*t^2  + c2*t  + c3 = 0 when
  // c1, c2, and c3 are REAL.
  // Solution is motivated by Numerical Recipes In C 2nd Ed.
  // Roots and number of roots are stored in user provided variables
  // r1, r2, num_roots
  // @deprecated Replaced by vtkPolynomialSolversUnivariate::SolveQuadratic() as
  // of VTK 5.8.
  VTK_LEGACY(static int SolveQuadratic(BT_DOUBLE c0, BT_DOUBLE c1, BT_DOUBLE c2,
                            BT_DOUBLE *r1, BT_DOUBLE *r2, int *num_roots));

  // Description:
  // Algebraically extracts REAL roots of the quadratic polynomial with
  // REAL coefficients c[0] X^2 + c[1] X + c[2]
  // and stores them (when they exist) and their respective multiplicities
  // in the \a r and \a m arrays.
  // Returns either the number of roots, or -1 if ininite number of roots.
  // @deprecated Replaced by vtkPolynomialSolversUnivariate::SolveQuadratic() as
  // of VTK 5.8.
  VTK_LEGACY(static int SolveQuadratic( BT_DOUBLE* c, BT_DOUBLE* r, int* m ));

  // Description:
  // Solves a linear equation c2*t + c3 = 0 when c2 and c3 are REAL.
  // Solution is motivated by Numerical Recipes In C 2nd Ed.
  // Root and number of (real) roots are stored in user provided variables
  // r2 and num_roots.
  // @deprecated Replaced by vtkPolynomialSolversUnivariate::SolveLinear() as
  // of VTK 5.8.
  VTK_LEGACY(static int SolveLinear(BT_DOUBLE c0, BT_DOUBLE c1, BT_DOUBLE *r1, int *num_roots));

  // Description:
  // Solves for the least squares best fit matrix for the homogeneous equation X'M' = 0'.
  // Uses the method described on pages 40-41 of Computer Vision by
  // Forsyth and Ponce, which is that the solution is the eigenvector
  // associated with the minimum eigenvalue of T(X)X, where T(X) is the
  // transpose of X.
  // The inputs and output are transposed matrices.
  //    Dimensions: X' is numberOfSamples by xOrder,
  //                M' dimension is xOrder by yOrder.
  // M' should be pre-allocated. All matrices are row major. The resultant
  // matrix M' should be pre-multiplied to X' to get 0', or transposed and
  // then post multiplied to X to get 0
  static int SolveHomogeneousLeastSquares(int numberOfSamples, BT_DOUBLE **xt, int xOrder,
                                BT_DOUBLE **mt);


  // Description:
  // Solves for the least squares best fit matrix for the equation X'M' = Y'.
  // Uses pseudoinverse to get the ordinary least squares.
  // The inputs and output are transposed matrices.
  //    Dimensions: X' is numberOfSamples by xOrder,
  //                Y' is numberOfSamples by yOrder,
  //                M' dimension is xOrder by yOrder.
  // M' should be pre-allocated. All matrices are row major. The resultant
  // matrix M' should be pre-multiplied to X' to get Y', or transposed and
  // then post multiplied to X to get Y
  // By default, this method checks for the homogeneous condition where Y==0, and
  // if so, invokes SolveHomogeneousLeastSquares. For better performance when
  // the system is known not to be homogeneous, invoke with checkHomogeneous=0.
  static int SolveLeastSquares(int numberOfSamples, BT_DOUBLE **xt, int xOrder,
                               BT_DOUBLE **yt, int yOrder, BT_DOUBLE **mt, int checkHomogeneous=1);

  // Description:
  // Convert color in RGB format (Red, Green, Blue) to HSV format
  // (Hue, Saturation, Value). The input color is not modified.
  // The input RGB must be BT_FLOAT values in the range [0,1].
  // The output ranges are hue [0, 1], saturation [0, 1], and
  // value [0, 1].
  static void RGBToHSV(const BT_FLOAT rgb[3], BT_FLOAT hsv[3])
    { RGBToHSV(rgb[0], rgb[1], rgb[2], hsv, hsv+1, hsv+2); }
  static void RGBToHSV(BT_FLOAT r, BT_FLOAT g, BT_FLOAT b, BT_FLOAT *h, BT_FLOAT *s, BT_FLOAT *v);
  static BT_DOUBLE* RGBToHSV(const BT_DOUBLE rgb[3]);
  static BT_DOUBLE* RGBToHSV(BT_DOUBLE r, BT_DOUBLE g, BT_DOUBLE b);
  static void RGBToHSV(const BT_DOUBLE rgb[3], BT_DOUBLE hsv[3])
    { RGBToHSV(rgb[0], rgb[1], rgb[2], hsv, hsv+1, hsv+2); }
  static void RGBToHSV(BT_DOUBLE r, BT_DOUBLE g, BT_DOUBLE b, BT_DOUBLE *h, BT_DOUBLE *s, BT_DOUBLE *v);

  // Description:
  // Convert color in HSV format (Hue, Saturation, Value) to RGB
  // format (Red, Green, Blue). The input color is not modified.
  static void HSVToRGB(const BT_FLOAT hsv[3], BT_FLOAT rgb[3])
    { HSVToRGB(hsv[0], hsv[1], hsv[2], rgb, rgb+1, rgb+2); }
  static void HSVToRGB(BT_FLOAT h, BT_FLOAT s, BT_FLOAT v, BT_FLOAT *r, BT_FLOAT *g, BT_FLOAT *b);
  static BT_DOUBLE* HSVToRGB(const BT_DOUBLE hsv[3]);
  static BT_DOUBLE* HSVToRGB(BT_DOUBLE h, BT_DOUBLE s, BT_DOUBLE v);
  static void HSVToRGB(const BT_DOUBLE hsv[3], BT_DOUBLE rgb[3])
    { HSVToRGB(hsv[0], hsv[1], hsv[2], rgb, rgb+1, rgb+2); }
  static void HSVToRGB(BT_DOUBLE h, BT_DOUBLE s, BT_DOUBLE v, BT_DOUBLE *r, BT_DOUBLE *g, BT_DOUBLE *b);

  // Description:
  // Convert color from the CIE-L*ab system to CIE XYZ.
  static void LabToXYZ(const BT_DOUBLE lab[3], BT_DOUBLE xyz[3]) {
    LabToXYZ(lab[0], lab[1], lab[2], xyz+0, xyz+1, xyz+2);
  }
  static void LabToXYZ(BT_DOUBLE L, BT_DOUBLE a, BT_DOUBLE b,
                       BT_DOUBLE *x, BT_DOUBLE *y, BT_DOUBLE *z);
  static BT_DOUBLE *LabToXYZ(const BT_DOUBLE lab[3]);

  // Description:
  // Convert Color from the CIE XYZ system to CIE-L*ab.
  static void XYZToLab(const BT_DOUBLE xyz[3], BT_DOUBLE lab[3]) {
    XYZToLab(xyz[0], xyz[1], xyz[2], lab+0, lab+1, lab+2);
  }
  static void XYZToLab(BT_DOUBLE x, BT_DOUBLE y, BT_DOUBLE z,
                       BT_DOUBLE *L, BT_DOUBLE *a, BT_DOUBLE *b);
  static BT_DOUBLE *XYZToLab(const BT_DOUBLE xyz[3]);

  // Description:
  // Convert color from the CIE XYZ system to RGB.
  static void XYZToRGB(const BT_DOUBLE xyz[3], BT_DOUBLE rgb[3]) {
    XYZToRGB(xyz[0], xyz[1], xyz[2], rgb+0, rgb+1, rgb+2);
  }
  static void XYZToRGB(BT_DOUBLE x, BT_DOUBLE y, BT_DOUBLE z,
                       BT_DOUBLE *r, BT_DOUBLE *g, BT_DOUBLE *b);
  static BT_DOUBLE *XYZToRGB(const BT_DOUBLE xyz[3]);

  // Description:
  // Convert color from the RGB system to CIE XYZ.
  static void RGBToXYZ(const BT_DOUBLE rgb[3], BT_DOUBLE xyz[3]) {
    RGBToXYZ(rgb[0], rgb[1], rgb[2], xyz+0, xyz+1, xyz+2);
  }
  static void RGBToXYZ(BT_DOUBLE r, BT_DOUBLE g, BT_DOUBLE b,
                       BT_DOUBLE *x, BT_DOUBLE *y, BT_DOUBLE *z);
  static BT_DOUBLE *RGBToXYZ(const BT_DOUBLE rgb[3]);

  // Description:
  // Convert color from the RGB system to CIE-L*ab.
  // The input RGB must be values in the range [0,1].
  // The output ranges of 'L' is [0, 100]. The output
  // range of 'a' and 'b' are approximately [-110, 110].
  static void RGBToLab(const BT_DOUBLE rgb[3], BT_DOUBLE lab[3]) {
    RGBToLab(rgb[0], rgb[1], rgb[2], lab+0, lab+1, lab+2);
  }
  static void RGBToLab(BT_DOUBLE red, BT_DOUBLE green, BT_DOUBLE blue,
                       BT_DOUBLE *L, BT_DOUBLE *a, BT_DOUBLE *b);
  static BT_DOUBLE *RGBToLab(const BT_DOUBLE rgb[3]);

  // Description:
  // Convert color from the CIE-L*ab system to RGB.
  static void LabToRGB(const BT_DOUBLE lab[3], BT_DOUBLE rgb[3]) {
    LabToRGB(lab[0], lab[1], lab[2], rgb+0, rgb+1, rgb+2);
  }
  static void LabToRGB(BT_DOUBLE L, BT_DOUBLE a, BT_DOUBLE b,
                       BT_DOUBLE *red, BT_DOUBLE *green, BT_DOUBLE *blue);
  static BT_DOUBLE *LabToRGB(const BT_DOUBLE lab[3]);

  // Description:
  // Set the bounds to an uninitialized state
  static void UninitializeBounds(BT_DOUBLE bounds[6]){
    bounds[0] = 1.0;
    bounds[1] = -1.0;
    bounds[2] = 1.0;
    bounds[3] = -1.0;
    bounds[4] = 1.0;
    bounds[5] = -1.0;
  }

  // Description:
  // Are the bounds initialized?
  static int AreBoundsInitialized(BT_DOUBLE bounds[6]){
    if ( bounds[1]-bounds[0]<0.0 )
      {
      return 0;
      }
    return 1;
  }

  // Description:
  // Clamp some values against a range
  // The method without 'clamped_values' will perform in-place clamping.
  static void ClampValue(BT_DOUBLE *value, const BT_DOUBLE range[2]);
  static void ClampValue(BT_DOUBLE value, const BT_DOUBLE range[2], BT_DOUBLE *clamped_value);
  static void ClampValues(
    BT_DOUBLE *values, int nb_values, const BT_DOUBLE range[2]);
  static void ClampValues(
    const BT_DOUBLE *values, int nb_values, const BT_DOUBLE range[2], BT_DOUBLE *clamped_values);

  // Description:
  // Clamp a value against a range and then normalized it between 0 and 1.
  // If range[0]==range[1], the result is 0.
  // \pre valid_range: range[0]<=range[1]
  // \post valid_result: result>=0.0 && result<=1.0
  static BT_DOUBLE ClampAndNormalizeValue(BT_DOUBLE value,
                                       const BT_DOUBLE range[2]);

  // Description:
  // Return the scalar type that is most likely to have enough precision
  // to store a given range of data once it has been scaled and shifted
  // (i.e. [range_min * scale + shift, range_max * scale + shift].
  // If any one of the parameters is not an integer number (decimal part != 0),
  // the search will default to BT_FLOAT types only (BT_FLOAT or BT_DOUBLE)
  // Return -1 on error or no scalar type found.
  static int GetScalarTypeFittingRange(
    BT_DOUBLE range_min, BT_DOUBLE range_max,
    BT_DOUBLE scale = 1.0, BT_DOUBLE shift = 0.0);

  // Description:
  // Get a vtkDataArray's scalar range for a given component.
  // If the vtkDataArray's data type is unsigned char (VTK_UNSIGNED_CHAR)
  // the range is adjusted to the whole data type range [0, 255.0].
  // Same goes for unsigned short (VTK_UNSIGNED_SHORT) but the upper bound
  // is also adjusted down to 4095.0 if was between ]255, 4095.0].
  // Return 1 on success, 0 otherwise.
  static int GetAdjustedScalarRange(
    vtkDataArray *array, int comp, BT_DOUBLE range[2]);

  // Description:
  // Return true if first 3D extent is within second 3D extent
  // Extent is x-min, x-max, y-min, y-max, z-min, z-max
  static int ExtentIsWithinOtherExtent(int extent1[6], int extent2[6]);

  // Description:
  // Return true if first 3D bounds is within the second 3D bounds
  // Bounds is x-min, x-max, y-min, y-max, z-min, z-max
  // Delta is the error margin along each axis (usually a small number)
  static int BoundsIsWithinOtherBounds(BT_DOUBLE bounds1[6], BT_DOUBLE bounds2[6], BT_DOUBLE delta[3]);

  // Description:
  // Return true if point is within the given 3D bounds
  // Bounds is x-min, x-max, y-min, y-max, z-min, z-max
  // Delta is the error margin along each axis (usually a small number)
  static int PointIsWithinBounds(BT_DOUBLE point[3], BT_DOUBLE bounds[6], BT_DOUBLE delta[3]);

  // Description:
  // In Euclidean space, there is a unique circle passing through any given
  // three non-collinear points P1, P2, and P3. Using Cartesian coordinates
  // to represent these points as spatial vectors, it is possible to use the
  // dot product and cross product to calculate the radius and center of the
  // circle. See: http://en.wikipedia.org/wiki/Circumcircle and more
  // specifically the section Barycentric coordinates from cross- and
  // dot-products
  static BT_DOUBLE Solve3PointCircle(const BT_DOUBLE p1[3], const BT_DOUBLE p2[3], const BT_DOUBLE p3[3], BT_DOUBLE center[3]);

  // Description:
  // Special IEEE-754 number used to represent positive infinity.
  static BT_DOUBLE Inf();

  // Description:
  // Special IEEE-754 number used to represent negative infinity.
  static BT_DOUBLE NegInf();

  // Description:
  // Special IEEE-754 number used to represent Not-A-Number (Nan).
  static BT_DOUBLE Nan();

  // Description:
  // Test if a number is equal to the special floating point value infinity.
  static int IsInf(BT_DOUBLE x);

  // Description:
  // Test if a number is equal to the special floating point value Not-A-Number (Nan).
  static int IsNan(BT_DOUBLE x);

protected:
  vtkMath() {};
  ~vtkMath() {};

  static vtkMathInternal Internal;
private:
  vtkMath(const vtkMath&);  // Not implemented.
  void operator=(const vtkMath&);  // Not implemented.
};

//----------------------------------------------------------------------------
inline BT_FLOAT vtkMath::RadiansFromDegrees( BT_FLOAT x )
{
  return x * 0.017453292f;
}

//----------------------------------------------------------------------------
inline BT_DOUBLE vtkMath::RadiansFromDegrees( BT_DOUBLE x )
{
  return x * 0.017453292519943295;
}

//----------------------------------------------------------------------------
inline BT_FLOAT vtkMath::DegreesFromRadians( BT_FLOAT x )
{
  return x * 57.2957795131f;
}

//----------------------------------------------------------------------------
inline BT_DOUBLE vtkMath::DegreesFromRadians( BT_DOUBLE x )
{
  return x * 57.29577951308232;
}

//----------------------------------------------------------------------------
inline vtkTypeInt64 vtkMath::Factorial( int N )
{
  vtkTypeInt64 r = 1;
  while ( N > 1 )
    {
    r *= N--;
    }
  return r;
}

//----------------------------------------------------------------------------
// Modify the trunc() operation provided by performCast<int>() to get floor(),
// if x<0 (condition g) and x!=trunc(x) (condition n) then floor(x)=trunc(x)-1
// Note that in C++ conditions evaluate to values of 1 or 0 (true or false).
inline int vtkMath::Floor(BT_DOUBLE x)
{
  const int r = performCast<int>(x);
  const int n = ( x != performCast<BT_DOUBLE>(r) );
  const int g = ( x < 0 );
  return r - ( n & g );
}

//----------------------------------------------------------------------------
// Modify the trunc() operation provided by performCast<int>() to get ceil(),
// if x>=0 (condition g) and x!=trunc(x) (condition n) then ceil(x)=trunc(x)+1
// Note that in C++ conditions evaluate to values of 1 or 0 (true or false).
inline int vtkMath::Ceil(BT_DOUBLE x)
{
  const int r = performCast<int>(x);
  const int n = ( x != performCast<BT_DOUBLE>(r) );
  const int g = ( x >= 0 );
  return r + ( n & g );
}

//----------------------------------------------------------------------------
inline BT_FLOAT vtkMath::Normalize(BT_FLOAT x[3])
{
  BT_FLOAT den;
  if ( ( den = vtkMath::Norm( x ) ) != 0.0 )
    {
    for (int i=0; i < 3; i++)
      {
      x[i] /= den;
      }
    }
  return den;
}

//----------------------------------------------------------------------------
inline BT_DOUBLE vtkMath::Normalize(BT_DOUBLE x[3])
{
  BT_DOUBLE den;
  if ( ( den = vtkMath::Norm( x ) ) != 0.0 )
    {
    for (int i=0; i < 3; i++)
      {
      x[i] /= den;
      }
    }
  return den;
}

//----------------------------------------------------------------------------
inline BT_FLOAT vtkMath::Normalize2D(BT_FLOAT x[3])
{
  BT_FLOAT den;
  if ( ( den = vtkMath::Norm2D( x ) ) != 0.0 )
    {
    for (int i=0; i < 2; i++)
      {
      x[i] /= den;
      }
    }
  return den;
}

//----------------------------------------------------------------------------
inline BT_DOUBLE vtkMath::Normalize2D(BT_DOUBLE x[3])
{
  BT_DOUBLE den;
  if ( ( den = vtkMath::Norm2D( x ) ) != 0.0 )
    {
    for (int i=0; i < 2; i++)
      {
      x[i] /= den;
      }
    }
  return den;
}

//----------------------------------------------------------------------------
inline BT_FLOAT vtkMath::Determinant3x3(const BT_FLOAT c1[3],
                                     const BT_FLOAT c2[3],
                                     const BT_FLOAT c3[3])
{
  return c1[0] * c2[1] * c3[2] + c2[0] * c3[1] * c1[2] + c3[0] * c1[1] * c2[2] -
         c1[0] * c3[1] * c2[2] - c2[0] * c1[1] * c3[2] - c3[0] * c2[1] * c1[2];
}

//----------------------------------------------------------------------------
inline BT_DOUBLE vtkMath::Determinant3x3(const BT_DOUBLE c1[3],
                                      const BT_DOUBLE c2[3],
                                      const BT_DOUBLE c3[3])
{
  return c1[0] * c2[1] * c3[2] + c2[0] * c3[1] * c1[2] + c3[0] * c1[1] * c2[2] -
         c1[0] * c3[1] * c2[2] - c2[0] * c1[1] * c3[2] - c3[0] * c2[1] * c1[2];
}

//----------------------------------------------------------------------------
inline BT_DOUBLE vtkMath::Determinant3x3(BT_DOUBLE a1, BT_DOUBLE a2, BT_DOUBLE a3,
                                      BT_DOUBLE b1, BT_DOUBLE b2, BT_DOUBLE b3,
                                      BT_DOUBLE c1, BT_DOUBLE c2, BT_DOUBLE c3)
{
    return ( a1 * vtkMath::Determinant2x2( b2, b3, c2, c3 )
           - b1 * vtkMath::Determinant2x2( a2, a3, c2, c3 )
           + c1 * vtkMath::Determinant2x2( a2, a3, b2, b3 ) );
}

//----------------------------------------------------------------------------
inline BT_FLOAT vtkMath::Distance2BetweenPoints(const BT_FLOAT x[3],
                                             const BT_FLOAT y[3])
{
  return ( ( x[0] - y[0] ) * ( x[0] - y[0] )
           + ( x[1] - y[1] ) * ( x[1] - y[1] )
           + ( x[2] - y[2] ) * ( x[2] - y[2] ) );
}

//----------------------------------------------------------------------------
inline BT_DOUBLE vtkMath::Distance2BetweenPoints(const BT_DOUBLE x[3],
                                              const BT_DOUBLE y[3])
{
  return ( ( x[0] - y[0] ) * ( x[0] - y[0] )
           + ( x[1] - y[1] ) * ( x[1] - y[1] )
           + ( x[2] - y[2] ) * ( x[2] - y[2] ) );
}

//----------------------------------------------------------------------------
// Cross product of two 3-vectors. Result (a x b) is stored in z[3].
inline void vtkMath::Cross(const BT_FLOAT x[3], const BT_FLOAT y[3], BT_FLOAT z[3])
{
  BT_FLOAT Zx = x[1] * y[2] - x[2] * y[1];
  BT_FLOAT Zy = x[2] * y[0] - x[0] * y[2];
  BT_FLOAT Zz = x[0] * y[1] - x[1] * y[0];
  z[0] = Zx; z[1] = Zy; z[2] = Zz;
}

//----------------------------------------------------------------------------
// Cross product of two 3-vectors. Result (a x b) is stored in z[3].
inline void vtkMath::Cross(const BT_DOUBLE x[3], const BT_DOUBLE y[3], BT_DOUBLE z[3])
{
  BT_DOUBLE Zx = x[1] * y[2] - x[2] * y[1];
  BT_DOUBLE Zy = x[2] * y[0] - x[0] * y[2];
  BT_DOUBLE Zz = x[0] * y[1] - x[1] * y[0];
  z[0] = Zx; z[1] = Zy; z[2] = Zz;
}

//BTX
//----------------------------------------------------------------------------
template<class T>
inline BT_DOUBLE vtkDeterminant3x3(T A[3][3])
{
  return (BT_DOUBLE)(A[0][0] * A[1][1] * A[2][2] + A[1][0] * A[2][1] * A[0][2] +
         A[2][0] * A[0][1] * A[1][2] - A[0][0] * A[2][1] * A[1][2] -
         A[1][0] * A[0][1] * A[2][2] - A[2][0] * A[1][1] * A[0][2]);
}
//ETX

//----------------------------------------------------------------------------
inline BT_DOUBLE vtkMath::Determinant3x3(BT_FLOAT A[3][3])
{
  return vtkDeterminant3x3( A );
}

//----------------------------------------------------------------------------
inline BT_DOUBLE vtkMath::Determinant3x3(BT_DOUBLE A[3][3])
{
  return vtkDeterminant3x3( A );
}

#ifndef VTK_LEGACY_REMOVE
//----------------------------------------------------------------------------
inline BT_DOUBLE* vtkMath::SolveCubic(BT_DOUBLE c0, BT_DOUBLE c1, BT_DOUBLE c2, BT_DOUBLE c3)
{
  VTK_LEGACY_REPLACED_BODY(vtkMath::SolveCubic, "VTK 5.8",
                           vtkPolynomialSolversUnivariate::SolveCubic);
  return vtkPolynomialSolversUnivariate::SolveCubic( c0, c1, c2, c3 );
}

//----------------------------------------------------------------------------
inline BT_DOUBLE* vtkMath::SolveQuadratic(BT_DOUBLE c0, BT_DOUBLE c1, BT_DOUBLE c2)
{
  VTK_LEGACY_REPLACED_BODY(vtkMath::SolveQuadratic, "VTK 5.8",
                           vtkPolynomialSolversUnivariate::SolveQuadratic);
  return vtkPolynomialSolversUnivariate::SolveQuadratic( c0, c1, c2 );
}

//----------------------------------------------------------------------------
inline BT_DOUBLE* vtkMath::SolveLinear(BT_DOUBLE c0, BT_DOUBLE c1)
{
  VTK_LEGACY_REPLACED_BODY(vtkMath::SolveLinear, "VTK 5.8",
                           vtkPolynomialSolversUnivariate::SolveLinear);
  return vtkPolynomialSolversUnivariate::SolveLinear( c0, c1 );
}

//----------------------------------------------------------------------------
inline int vtkMath::SolveCubic(BT_DOUBLE c0, BT_DOUBLE c1, BT_DOUBLE c2, BT_DOUBLE c3,
                               BT_DOUBLE *r1, BT_DOUBLE *r2, BT_DOUBLE *r3, int *num_roots)
{
  VTK_LEGACY_REPLACED_BODY(vtkMath::SolveCubic, "VTK 5.8",
                           vtkPolynomialSolversUnivariate::SolveCubic);
  return vtkPolynomialSolversUnivariate::SolveCubic( c0, c1, c2, c3, r1, r2, r3, num_roots );
}

//----------------------------------------------------------------------------
inline int vtkMath::SolveQuadratic(BT_DOUBLE c0, BT_DOUBLE c1, BT_DOUBLE c2,
                                   BT_DOUBLE *r1, BT_DOUBLE *r2, int *num_roots)
{
  VTK_LEGACY_REPLACED_BODY(vtkMath::SolveQuadratic, "VTK 5.8",
                           vtkPolynomialSolversUnivariate::SolveQuadratic);
  return vtkPolynomialSolversUnivariate::SolveQuadratic( c0, c1, c2, r1, r2, num_roots );
}

//----------------------------------------------------------------------------
inline int vtkMath::SolveQuadratic( BT_DOUBLE* c, BT_DOUBLE* r, int* m )
{
  VTK_LEGACY_REPLACED_BODY(vtkMath::SolveQuadratic, "VTK 5.8",
                           vtkPolynomialSolversUnivariate::SolveQuadratic);
  return vtkPolynomialSolversUnivariate::SolveQuadratic( c, r, m );
}

//----------------------------------------------------------------------------
inline int vtkMath::SolveLinear(BT_DOUBLE c0, BT_DOUBLE c1, BT_DOUBLE *r1, int *num_roots)
{
  VTK_LEGACY_REPLACED_BODY(vtkMath::SolveLinear, "VTK 5.8",
                           vtkPolynomialSolversUnivariate::SolveLinear);
  return vtkPolynomialSolversUnivariate::SolveLinear( c0, c1, r1, num_roots );
}
#endif

//----------------------------------------------------------------------------
inline void vtkMath::ClampValue(BT_DOUBLE *value, const BT_DOUBLE range[2])
{
  if (value && range)
    {
    if (*value < range[0])
      {
      *value = range[0];
      }
    else if (*value > range[1])
      {
      *value = range[1];
      }
    }
}

//----------------------------------------------------------------------------
inline void vtkMath::ClampValue(
  BT_DOUBLE value, const BT_DOUBLE range[2], BT_DOUBLE *clamped_value)
{
  if (range && clamped_value)
    {
    if (value < range[0])
      {
      *clamped_value = range[0];
      }
    else if (value > range[1])
      {
      *clamped_value = range[1];
      }
    else
      {
      *clamped_value = value;
      }
    }
}

// ---------------------------------------------------------------------------
inline BT_DOUBLE vtkMath::ClampAndNormalizeValue(BT_DOUBLE value,
                                              const BT_DOUBLE range[2])
{
  assert("pre: valid_range" && range[0]<=range[1]);

  BT_DOUBLE result;
  if(range[0]==range[1])
    {
      result=0.0;
    }
  else
    {
      // clamp
      if(value<range[0])
        {
          result=range[0];
        }
      else
        {
          if(value>range[1])
            {
              result=range[1];
            }
          else
            {
              result=value;
            }
        }

      // normalize
      result=( result - range[0] ) / ( range[1] - range[0] );
    }

  assert("post: valid_result" && result>=0.0 && result<=1.0);

  return result;
}

#if defined(VTK_HAS_ISINF)
#ifndef __WRAP__ // Fixes bug 13443 for VTK 5.10
//-----------------------------------------------------------------------------
inline int vtkMath::IsInf(BT_DOUBLE x)
{
  return (isinf(x) ? 1 : 0);
}
#endif
#endif

#if defined(VTK_HAS_ISNAN)
#ifndef __WRAP__ // Fixes bug 13443 for VTK 5.10
//-----------------------------------------------------------------------------
inline int vtkMath::IsNan(BT_DOUBLE x)
{
  return (isnan(x) ? 1 : 0);
}
#endif
#endif

#endif
