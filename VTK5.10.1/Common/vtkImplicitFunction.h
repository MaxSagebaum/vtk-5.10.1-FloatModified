/*=========================================================================

  Program:   Visualization Toolkit
  Module:    vtkImplicitFunction.h

  Copyright (c) Ken Martin, Will Schroeder, Bill Lorensen
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/Copyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkImplicitFunction - abstract interface for implicit functions
// .SECTION Description
// vtkImplicitFunction specifies an abstract interface for implicit
// functions. Implicit functions are real valued functions defined in 3D
// space, w = F(x,y,z). Two primitive operations are required: the ability to
// evaluate the function, and the function gradient at a given point. The 
// implicit function divides space into three regions: on the surface
// (F(x,y,z)=w), outside of the surface (F(x,y,z)>c), and inside the
// surface (F(x,y,z)<c). (When c is zero, positive values are outside,
// negative values are inside, and zero is on the surface. Note also
// that the function gradient points from inside to outside.)
//
// Implicit functions are very powerful. It is possible to represent almost
// any type of geometry with the level sets w = const, especially if you use 
// boolean combinations of implicit functions (see vtkImplicitBoolean).
//
// vtkImplicitFunction provides a mechanism to transform the implicit
// function(s) via a vtkAbstractTransform.  This capability can be used to 
// translate, orient, scale, or warp implicit functions.  For example, 
// a sphere implicit function can be transformed into an oriented ellipse. 

// .SECTION Caveats
// The transformation transforms a point into the space of the implicit
// function (i.e., the model space). Typically we want to transform the 
// implicit model into world coordinates. In this case the inverse of the 
// transformation is required.

// .SECTION See Also
// vtkAbstractTransform vtkSphere vtkCylinder vtkImplicitBoolean vtkPlane 
// vtkPlanes vtkQuadric vtkImplicitVolume vtkSampleFunction vtkCutter
// vtkClipPolyData

#ifndef __vtkImplicitFunction_h
#define __vtkImplicitFunction_h

#include "vtkObject.h"

class vtkAbstractTransform;

class VTK_COMMON_EXPORT vtkImplicitFunction : public vtkObject
{
public:
  vtkTypeMacro(vtkImplicitFunction,vtkObject);
  void PrintSelf(ostream& os, vtkIndent indent);

  // Description:
  // Overload standard modified time function. If Transform is modified,
  // then this object is modified as well.
  unsigned long GetMTime();

  // Description:
  // Evaluate function at position x-y-z and return value. Point x[3] is
  // transformed through transform (if provided).
  BT_DOUBLE FunctionValue(const BT_DOUBLE x[3]);
  BT_DOUBLE FunctionValue(BT_DOUBLE x, BT_DOUBLE y, BT_DOUBLE z) {
    BT_DOUBLE xyz[3] = {x, y, z}; return this->FunctionValue(xyz); };

  // Description:
  // Evaluate function gradient at position x-y-z and pass back vector. Point
  // x[3] is transformed through transform (if provided).
  void FunctionGradient(const BT_DOUBLE x[3], BT_DOUBLE g[3]);
  BT_DOUBLE *FunctionGradient(const BT_DOUBLE x[3]) {
    this->FunctionGradient(x,this->ReturnValue);
    return this->ReturnValue; };
  BT_DOUBLE *FunctionGradient(BT_DOUBLE x, BT_DOUBLE y, BT_DOUBLE z) {
    BT_DOUBLE xyz[3] = {x, y, z}; return this->FunctionGradient(xyz); };

  // Description:
  // Set/Get a transformation to apply to input points before
  // executing the implicit function.
  virtual void SetTransform(vtkAbstractTransform*);
  virtual void SetTransform(const BT_DOUBLE elements[16]);
  vtkGetObjectMacro(Transform,vtkAbstractTransform);

  // Description:
  // Evaluate function at position x-y-z and return value.  You should
  // generally not call this method directly, you should use 
  // FunctionValue() instead.  This method must be implemented by 
  // any derived class. 
  virtual BT_DOUBLE EvaluateFunction(BT_DOUBLE x[3]) = 0;
  BT_DOUBLE EvaluateFunction(BT_DOUBLE x, BT_DOUBLE y, BT_DOUBLE z) {
    BT_DOUBLE xyz[3] = {x, y, z}; return this->EvaluateFunction(xyz); };

  // Description:
  // Evaluate function gradient at position x-y-z and pass back vector. 
  // You should generally not call this method directly, you should use 
  // FunctionGradient() instead.  This method must be implemented by 
  // any derived class. 
  virtual void EvaluateGradient(BT_DOUBLE x[3], BT_DOUBLE g[3]) = 0;

protected:
  vtkImplicitFunction();
  ~vtkImplicitFunction();

  vtkAbstractTransform *Transform;
  BT_DOUBLE ReturnValue[3];
private:
  vtkImplicitFunction(const vtkImplicitFunction&);  // Not implemented.
  void operator=(const vtkImplicitFunction&);  // Not implemented.
};

#endif
