/*=========================================================================

  Program:   Visualization Toolkit
  Module:    vtkParametricEllipsoid.h

  Copyright (c) Ken Martin, Will Schroeder, Bill Lorensen 
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/Copyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even 
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR 
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkParametricEllipsoid - Generate an ellipsoid.
// .SECTION Description
// vtkParametricEllipsoid generates an ellipsoid.
// If all the radii are the same, we have a sphere.
// An oblate spheroid occurs if RadiusX = RadiusY > RadiusZ. 
// Here the Z-axis forms the symmetry axis. To a first
// approximation, this is the shape of the earth.
// A prolate spheroid occurs if RadiusX = RadiusY < RadiusZ.
//
// For further information about this surface, please consult the 
// technical description "Parametric surfaces" in http://www.vtk.org/documents.php 
// in the "VTK Technical Documents" section in the VTk.org web pages.
//
// .SECTION Thanks
// Andrew Maclean a.maclean@cas.edu.au for creating and contributing the
// class.
//
#ifndef __vtkParametricEllipsoid_h
#define __vtkParametricEllipsoid_h

#include "vtkParametricFunction.h"

class VTK_COMMON_EXPORT vtkParametricEllipsoid : public vtkParametricFunction
{
public:
  vtkTypeMacro(vtkParametricEllipsoid,vtkParametricFunction);
  void PrintSelf(ostream& os, vtkIndent indent);
  
  // Description:
  // Construct an ellipsoid with the following parameters:
  // MinimumU = 0, MaximumU = 2*Pi, 
  // MinimumV = 0, MaximumV = Pi, 
  // JoinU = 1, JoinV = 0, 
  // TwistU = 0, TwistV = 0, 
  // ClockwiseOrdering = 1,
  // DerivativesAvailable = 1, 
  // XRadius = 1, YRadius = 1,
  // ZRadius = 1, a sphere in this case.
  static vtkParametricEllipsoid *New();

  // Description
  // Return the parametric dimension of the class.
  virtual int GetDimension() {return 2;}

  // Description:
  // Set/Get the scaling factor for the x-axis. Default = 1.
  vtkSetMacro(XRadius,BT_DOUBLE);
  vtkGetMacro(XRadius,BT_DOUBLE);

  // Description:
  // Set/Get the scaling factor for the y-axis. Default = 1.
  vtkSetMacro(YRadius,BT_DOUBLE);
  vtkGetMacro(YRadius,BT_DOUBLE);

  // Description:
  // Set/Get the scaling factor for the z-axis. Default = 1.
  vtkSetMacro(ZRadius,BT_DOUBLE);
  vtkGetMacro(ZRadius,BT_DOUBLE);

  // Description:
  // An ellipsoid.
  //
  // This function performs the mapping \f$f(u,v) \rightarrow (x,y,x)\f$, returning it
  // as Pt. It also returns the partial derivatives Du and Dv.
  // \f$Pt = (x, y, z), Du = (dx/du, dy/du, dz/du), Dv = (dx/dv, dy/dv, dz/dv)\f$ .
  // Then the normal is \f$N = Du X Dv\f$ .
  virtual void Evaluate(BT_DOUBLE uvw[3], BT_DOUBLE Pt[3], BT_DOUBLE Duvw[9]);

  // Description:
  // Calculate a user defined scalar using one or all of uvw, Pt, Duvw.
  //
  // uvw are the parameters with Pt being the the cartesian point, 
  // Duvw are the derivatives of this point with respect to u, v and w.
  // Pt, Duvw are obtained from Evaluate().
  //
  // This function is only called if the ScalarMode has the value
  // vtkParametricFunctionSource::SCALAR_FUNCTION_DEFINED
  //
  // If the user does not need to calculate a scalar, then the 
  // instantiated function should return zero. 
  //
  virtual BT_DOUBLE EvaluateScalar(BT_DOUBLE uvw[3], BT_DOUBLE Pt[3], BT_DOUBLE Duvw[9]);

protected:
  vtkParametricEllipsoid();
  ~vtkParametricEllipsoid();

  // Variables
  BT_DOUBLE XRadius;
  BT_DOUBLE YRadius;
  BT_DOUBLE ZRadius;
  BT_DOUBLE N1;
  BT_DOUBLE N2;

private:
  vtkParametricEllipsoid(const vtkParametricEllipsoid&);  // Not implemented.
  void operator=(const vtkParametricEllipsoid&);  // Not implemented.

};

#endif
