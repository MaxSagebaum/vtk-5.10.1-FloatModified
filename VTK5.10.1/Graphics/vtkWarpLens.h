/*=========================================================================

  Program:   Visualization Toolkit
  Module:    vtkWarpLens.h

  Copyright (c) Ken Martin, Will Schroeder, Bill Lorensen
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/Copyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkWarpLens - deform geometry by applying lens distortion
// .SECTION Description
// vtkWarpLens is a filter that modifies point coordinates by moving
// in accord with a lens distortion model.

#ifndef __vtkWarpLens_h
#define __vtkWarpLens_h

#include "vtkPointSetAlgorithm.h"

class VTK_GRAPHICS_EXPORT vtkWarpLens : public vtkPointSetAlgorithm
{
public:
  static vtkWarpLens *New();
  vtkTypeMacro(vtkWarpLens,vtkPointSetAlgorithm);
  void PrintSelf(ostream& os, vtkIndent indent);

  // Description:
  // Specify second order symmetric radial lens distortion parameter.
  // This is obsoleted by newer instance variables.
  void SetKappa(BT_DOUBLE kappa);
  BT_DOUBLE GetKappa();

  // Description:
  // Specify the center of radial distortion in pixels.
  // This is obsoleted by newer instance variables.
  void SetCenter(BT_DOUBLE centerX, BT_DOUBLE centerY);
  BT_DOUBLE *GetCenter();

  // Description:
  // Specify the calibrated principal point of the camera/lens
  vtkSetVector2Macro(PrincipalPoint,BT_DOUBLE);
  vtkGetVectorMacro(PrincipalPoint,BT_DOUBLE,2);

  // Description:
  // Specify the symmetric radial distortion parameters for the lens
  vtkSetMacro(K1,BT_DOUBLE);
  vtkGetMacro(K1,BT_DOUBLE);
  vtkSetMacro(K2,BT_DOUBLE);
  vtkGetMacro(K2,BT_DOUBLE);

  // Description:
  // Specify the decentering distortion parameters for the lens
  vtkSetMacro(P1,BT_DOUBLE);
  vtkGetMacro(P1,BT_DOUBLE);
  vtkSetMacro(P2,BT_DOUBLE);
  vtkGetMacro(P2,BT_DOUBLE);

  // Description:
  // Specify the imager format width / height in mm
  vtkSetMacro(FormatWidth,BT_DOUBLE);
  vtkGetMacro(FormatWidth,BT_DOUBLE);
  vtkSetMacro(FormatHeight,BT_DOUBLE);
  vtkGetMacro(FormatHeight,BT_DOUBLE);

  // Description:
  // Specify the image width / height in pixels
  vtkSetMacro(ImageWidth,int);
  vtkGetMacro(ImageWidth,int);
  vtkSetMacro(ImageHeight,int);
  vtkGetMacro(ImageHeight,int);

  int FillInputPortInformation(int port, vtkInformation *info);

protected:
  vtkWarpLens();
  ~vtkWarpLens() {};

  int RequestDataObject(vtkInformation *request,
                        vtkInformationVector **inputVector,
                        vtkInformationVector *outputVector);
  int RequestData(vtkInformation *,
                  vtkInformationVector **,
                  vtkInformationVector *);

  BT_DOUBLE PrincipalPoint[2];      // The calibrated principal point of camera/lens in mm
  BT_DOUBLE K1;                     // Symmetric radial distortion parameters
  BT_DOUBLE K2;
  BT_DOUBLE P1;                     // Decentering distortion parameters
  BT_DOUBLE P2;
  BT_DOUBLE FormatWidth;            // imager format width in mm
  BT_DOUBLE FormatHeight;           // imager format height in mm
  int ImageWidth;               // image width in pixels
  int ImageHeight;              // image height in pixels
private:
  vtkWarpLens(const vtkWarpLens&);  // Not implemented.
  void operator=(const vtkWarpLens&);  // Not implemented.
};

#endif
