/*=========================================================================
  
Program:   Visualization Toolkit
Module:    vtkSectorSource.h

Copyright (c) Ken Martin, Will Schroeder, Bill Lorensen
All rights reserved.
See Copyright.txt or http://www.kitware.com/Copyright.htm for details.

This software is distributed WITHOUT ANY WARRANTY; without even
the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkSectorSource - create a sector of a disk
// .SECTION Description
// vtkSectorSource creates a sector of a polygonal disk. The 
// disk has zero height. The user can specify the inner and outer radius
// of the disk, the z-coordinate, and the radial and 
// circumferential resolution of the polygonal representation. 
// .SECTION See Also
// vtkLinearExtrusionFilter

#ifndef __vtkSectorSource_h
#define __vtkSectorSource_h

#include "vtkPolyDataAlgorithm.h"

class VTK_GRAPHICS_EXPORT vtkSectorSource : public vtkPolyDataAlgorithm 
{
public:
  static vtkSectorSource *New();
  vtkTypeMacro(vtkSectorSource,vtkPolyDataAlgorithm);
  void PrintSelf(ostream& os, vtkIndent indent);
  
  // Description:
  // Specify inner radius of the sector.
  vtkSetClampMacro(InnerRadius,BT_DOUBLE,0.0,VTK_DOUBLE_MAX)
    vtkGetMacro(InnerRadius,BT_DOUBLE);
  
  // Description:
  // Specify outer radius of the sector.
  vtkSetClampMacro(OuterRadius,BT_DOUBLE,0.0,VTK_DOUBLE_MAX)
    vtkGetMacro(OuterRadius,BT_DOUBLE);
  
  // Description:
  // Specify the z coordinate of the sector.
  vtkSetClampMacro(ZCoord,BT_DOUBLE,0.0,VTK_DOUBLE_MAX)
    vtkGetMacro(ZCoord,BT_DOUBLE);
  
  // Description:
  // Set the number of points in radius direction.
  vtkSetClampMacro(RadialResolution,int,1,VTK_LARGE_INTEGER)
    vtkGetMacro(RadialResolution,int);
  
  // Description:
  // Set the number of points in circumferential direction.
  vtkSetClampMacro(CircumferentialResolution,int,3,VTK_LARGE_INTEGER)
    vtkGetMacro(CircumferentialResolution,int);
  
  // Description:
  // Set the start angle of the sector.
  vtkSetClampMacro(StartAngle,BT_DOUBLE,0.0,VTK_DOUBLE_MAX)
    vtkGetMacro(StartAngle,BT_DOUBLE);
  
  // Description:
  // Set the end angle of the sector.
  vtkSetClampMacro(EndAngle,BT_DOUBLE,0.0,VTK_DOUBLE_MAX)
    vtkGetMacro(EndAngle,BT_DOUBLE);
  
protected:
  vtkSectorSource();
  ~vtkSectorSource() {};
  
  int RequestData(vtkInformation *, vtkInformationVector **, vtkInformationVector *);
  BT_DOUBLE InnerRadius;
  BT_DOUBLE OuterRadius;
  BT_DOUBLE ZCoord;
  int RadialResolution;
  int CircumferentialResolution;
  BT_DOUBLE StartAngle;
  BT_DOUBLE EndAngle;
  
private:
  vtkSectorSource(const vtkSectorSource&);  // Not implemented.
  void operator=(const vtkSectorSource&);  // Not implemented.
};

#endif
