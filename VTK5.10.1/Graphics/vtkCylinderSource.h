/*=========================================================================

  Program:   Visualization Toolkit
  Module:    vtkCylinderSource.h

  Copyright (c) Ken Martin, Will Schroeder, Bill Lorensen
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/Copyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkCylinderSource - generate a cylinder centered at origin
// .SECTION Description
// vtkCylinderSource creates a polygonal cylinder centered at Center;
// The axis of the cylinder is aligned along the global y-axis.
// The height and radius of the cylinder can be specified, as well as the
// number of sides. It is also possible to control whether the cylinder is
// open-ended or capped.

#ifndef __vtkCylinderSource_h
#define __vtkCylinderSource_h

#include "vtkPolyDataAlgorithm.h"

#include "vtkCell.h" // Needed for VTK_CELL_SIZE

class VTK_GRAPHICS_EXPORT vtkCylinderSource : public vtkPolyDataAlgorithm 
{
public:
  static vtkCylinderSource *New();
  vtkTypeMacro(vtkCylinderSource,vtkPolyDataAlgorithm);
  void PrintSelf(ostream& os, vtkIndent indent);

  // Description:
  // Set the height of the cylinder. Initial value is 1.
  vtkSetClampMacro(Height,BT_DOUBLE,0.0,VTK_DOUBLE_MAX)
  vtkGetMacro(Height,BT_DOUBLE);

  // Description:
  // Set the radius of the cylinder. Initial value is 0.5
  vtkSetClampMacro(Radius,BT_DOUBLE,0.0,VTK_DOUBLE_MAX)
  vtkGetMacro(Radius,BT_DOUBLE);

  // Description:
  // Set/Get cylinder center. Initial value is (0.0,0.0,0.0)
  vtkSetVector3Macro(Center,BT_DOUBLE);
  vtkGetVectorMacro(Center,BT_DOUBLE,3);

  // Description:
  // Set the number of facets used to define cylinder. Initial value is 6.
  vtkSetClampMacro(Resolution,int,2,VTK_CELL_SIZE)
  vtkGetMacro(Resolution,int);

  // Description:
  // Turn on/off whether to cap cylinder with polygons. Initial value is true.
  vtkSetMacro(Capping,int);
  vtkGetMacro(Capping,int);
  vtkBooleanMacro(Capping,int);

protected:
  vtkCylinderSource(int res=6);
  ~vtkCylinderSource() {};

  int RequestData(vtkInformation *, vtkInformationVector **, vtkInformationVector *);
  BT_DOUBLE Height;
  BT_DOUBLE Radius;
  BT_DOUBLE Center[3];
  int Resolution;
  int Capping;

private:
  vtkCylinderSource(const vtkCylinderSource&);  // Not implemented.
  void operator=(const vtkCylinderSource&);  // Not implemented.
};

#endif
