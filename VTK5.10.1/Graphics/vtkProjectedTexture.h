/*=========================================================================

  Program:   Visualization Toolkit
  Module:    vtkProjectedTexture.h

  Copyright (c) Ken Martin, Will Schroeder, Bill Lorensen
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/Copyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkProjectedTexture - assign texture coordinates for a projected texture
// .SECTION Description
// vtkProjectedTexture assigns texture coordinates to a dataset as if
// the texture was projected from a slide projected located somewhere in the
// scene.  Methods are provided to position the projector and aim it at a 
// location, to set the width of the projector's frustum, and to set the
// range of texture coordinates assigned to the dataset.  
//
// Objects in the scene that appear behind the projector are also assigned
// texture coordinates; the projected image is left-right and top-bottom 
// flipped, much as a lens' focus flips the rays of light that pass through
// it.  A warning is issued if a point in the dataset falls at the focus
// of the projector.

#ifndef __vtkProjectedTexture_h
#define __vtkProjectedTexture_h

#include "vtkDataSetAlgorithm.h"

#define VTK_PROJECTED_TEXTURE_USE_PINHOLE 0
#define VTK_PROJECTED_TEXTURE_USE_TWO_MIRRORS 1

class VTK_GRAPHICS_EXPORT vtkProjectedTexture : public vtkDataSetAlgorithm 
{
public:
  static vtkProjectedTexture *New();
  vtkTypeMacro(vtkProjectedTexture,vtkDataSetAlgorithm);
  void PrintSelf(ostream& os, vtkIndent indent);

  // Description:
  // Set/Get the position of the focus of the projector.
  vtkSetVector3Macro(Position,BT_DOUBLE);
  vtkGetVectorMacro(Position,BT_DOUBLE,3);

  // Description:
  // Set/Get the focal point of the projector (a point that lies along
  // the center axis of the projector's frustum).
  void SetFocalPoint(BT_DOUBLE focalPoint[3]);
  void SetFocalPoint(BT_DOUBLE x, BT_DOUBLE y, BT_DOUBLE z);
  vtkGetVectorMacro(FocalPoint,BT_DOUBLE,3);

  // Description:
  // Set/Get the camera mode of the projection -- pinhole projection or
  // two mirror projection.
  vtkSetMacro(CameraMode, int);
  vtkGetMacro(CameraMode, int);
  void SetCameraModeToPinhole() {this->SetCameraMode(VTK_PROJECTED_TEXTURE_USE_PINHOLE);}
  void SetCameraModeToTwoMirror() {this->SetCameraMode(VTK_PROJECTED_TEXTURE_USE_TWO_MIRRORS);}

  // Description:
  // Set/Get the mirror separation for the two mirror system.
  vtkSetMacro(MirrorSeparation, BT_DOUBLE);
  vtkGetMacro(MirrorSeparation, BT_DOUBLE);

  // Description:
  // Get the normalized orientation vector of the projector.
  vtkGetVectorMacro(Orientation,BT_DOUBLE,3);
  
  // Description:
  // Set/Get the up vector of the projector.
  vtkSetVector3Macro(Up,BT_DOUBLE);
  vtkGetVectorMacro(Up,BT_DOUBLE,3);

  // Description:
  // Set/Get the aspect ratio of a perpendicular cross-section of the
  // the projector's frustum.  The aspect ratio consists of three 
  // numbers:  (x, y, z), where x is the width of the 
  // frustum, y is the height, and z is the perpendicular
  // distance from the focus of the projector.
  //
  // For example, if the source of the image is a pinhole camera with
  // view angle A, then you could set x=1, y=1, z=1/tan(A).
  vtkSetVector3Macro(AspectRatio,BT_DOUBLE);
  vtkGetVectorMacro(AspectRatio,BT_DOUBLE,3);

  // Description:
  // Specify s-coordinate range for texture s-t coordinate pair.
  vtkSetVector2Macro(SRange,BT_DOUBLE);
  vtkGetVectorMacro(SRange,BT_DOUBLE,2);

  // Description:
  // Specify t-coordinate range for texture s-t coordinate pair.
  vtkSetVector2Macro(TRange,BT_DOUBLE);
  vtkGetVectorMacro(TRange,BT_DOUBLE,2);
  
protected:
  vtkProjectedTexture();
  ~vtkProjectedTexture() {};

  int RequestData(vtkInformation *, vtkInformationVector **, vtkInformationVector *);
  void ComputeNormal();

  int CameraMode;

  BT_DOUBLE Position[3];
  BT_DOUBLE Orientation[3];
  BT_DOUBLE FocalPoint[3];
  BT_DOUBLE Up[3];
  BT_DOUBLE MirrorSeparation;
  BT_DOUBLE AspectRatio[3];
  BT_DOUBLE SRange[2];
  BT_DOUBLE TRange[2];
private:
  vtkProjectedTexture(const vtkProjectedTexture&);  // Not implemented.
  void operator=(const vtkProjectedTexture&);  // Not implemented.
};

#endif

