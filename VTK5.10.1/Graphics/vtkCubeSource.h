/*=========================================================================

  Program:   Visualization Toolkit
  Module:    vtkCubeSource.h

  Copyright (c) Ken Martin, Will Schroeder, Bill Lorensen
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/Copyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkCubeSource - create a polygonal representation of a cube
// .SECTION Description
// vtkCubeSource creates a cube centered at origin. The cube is represented
// with four-sided polygons. It is possible to specify the length, width, 
// and height of the cube independently.

#ifndef __vtkCubeSource_h
#define __vtkCubeSource_h

#include "vtkPolyDataAlgorithm.h"

class VTK_GRAPHICS_EXPORT vtkCubeSource : public vtkPolyDataAlgorithm 
{
public:
  static vtkCubeSource *New();
  vtkTypeMacro(vtkCubeSource,vtkPolyDataAlgorithm);
  void PrintSelf(ostream& os, vtkIndent indent);

  // Description:
  // Set the length of the cube in the x-direction.
  vtkSetClampMacro(XLength,BT_DOUBLE,0.0,VTK_DOUBLE_MAX);
  vtkGetMacro(XLength,BT_DOUBLE);

  // Description:
  // Set the length of the cube in the y-direction.
  vtkSetClampMacro(YLength,BT_DOUBLE,0.0,VTK_DOUBLE_MAX);
  vtkGetMacro(YLength,BT_DOUBLE);

  // Description:
  // Set the length of the cube in the z-direction.
  vtkSetClampMacro(ZLength,BT_DOUBLE,0.0,VTK_DOUBLE_MAX);
  vtkGetMacro(ZLength,BT_DOUBLE);

  // Description:
  // Set the center of the cube.
  vtkSetVector3Macro(Center,BT_DOUBLE);
  vtkGetVectorMacro(Center,BT_DOUBLE,3);

  // Description:
  // Convenience method allows creation of cube by specifying bounding box.
  void SetBounds(BT_DOUBLE xMin, BT_DOUBLE xMax,
                 BT_DOUBLE yMin, BT_DOUBLE yMax,
                 BT_DOUBLE zMin, BT_DOUBLE zMax);
  void SetBounds(BT_DOUBLE bounds[6]);

protected:
  vtkCubeSource(BT_DOUBLE xL=1.0, BT_DOUBLE yL=1.0, BT_DOUBLE zL=1.0);
  ~vtkCubeSource() {};

  int RequestData(vtkInformation *, vtkInformationVector **, vtkInformationVector *);
  BT_DOUBLE XLength;
  BT_DOUBLE YLength;
  BT_DOUBLE ZLength;
  BT_DOUBLE Center[3];
private:
  vtkCubeSource(const vtkCubeSource&);  // Not implemented.
  void operator=(const vtkCubeSource&);  // Not implemented.
};

#endif
