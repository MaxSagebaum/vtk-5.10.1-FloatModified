/*=========================================================================

  Program:   Visualization Toolkit
  Module:    vtkThresholdPoints.h

  Copyright (c) Ken Martin, Will Schroeder, Bill Lorensen
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/Copyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkThresholdPoints - extracts points whose scalar value satisfies threshold criterion
// .SECTION Description
// vtkThresholdPoints is a filter that extracts points from a dataset that 
// satisfy a threshold criterion. The criterion can take three forms:
// 1) greater than a particular value; 2) less than a particular value; or
// 3) between a particular value. The output of the filter is polygonal data.
// .SECTION See Also
// vtkThreshold

#ifndef __vtkThresholdPoints_h
#define __vtkThresholdPoints_h

#include "vtkPolyDataAlgorithm.h"

class VTK_GRAPHICS_EXPORT vtkThresholdPoints : public vtkPolyDataAlgorithm
{
public:
  static vtkThresholdPoints *New();
  vtkTypeMacro(vtkThresholdPoints,vtkPolyDataAlgorithm);
  void PrintSelf(ostream& os, vtkIndent indent);
  
  // Description:
  // Criterion is cells whose scalars are less or equal to lower threshold.
  void ThresholdByLower(BT_DOUBLE lower);

  // Description:
  // Criterion is cells whose scalars are greater or equal to upper threshold.
  void ThresholdByUpper(BT_DOUBLE upper);

  // Description:
  // Criterion is cells whose scalars are between lower and upper thresholds
  // (inclusive of the end values).
  void ThresholdBetween(BT_DOUBLE lower, BT_DOUBLE upper);

  // Description:
  // Set/Get the upper threshold.
  vtkSetMacro(UpperThreshold,BT_DOUBLE);
  vtkGetMacro(UpperThreshold,BT_DOUBLE);

  // Description:
  // Set/Get the lower threshold.
  vtkSetMacro(LowerThreshold,BT_DOUBLE);
  vtkGetMacro(LowerThreshold,BT_DOUBLE);

protected:
  vtkThresholdPoints();
  ~vtkThresholdPoints() {};

  // Usual data generation method
  virtual int RequestData(vtkInformation *, vtkInformationVector **, vtkInformationVector *);

  virtual int FillInputPortInformation(int port, vtkInformation *info);

  BT_DOUBLE LowerThreshold;
  BT_DOUBLE UpperThreshold;

  //BTX
  int (vtkThresholdPoints::*ThresholdFunction)(BT_DOUBLE s);
  //ETX

  int Lower(BT_DOUBLE s) {return ( s <= this->LowerThreshold ? 1 : 0 );};
  int Upper(BT_DOUBLE s) {return ( s >= this->UpperThreshold ? 1 : 0 );};
  int Between(BT_DOUBLE s) {return ( s >= this->LowerThreshold ? 
                               ( s <= this->UpperThreshold ? 1 : 0 ) : 0 );};
private:
  vtkThresholdPoints(const vtkThresholdPoints&);  // Not implemented.
  void operator=(const vtkThresholdPoints&);  // Not implemented.
};

#endif
