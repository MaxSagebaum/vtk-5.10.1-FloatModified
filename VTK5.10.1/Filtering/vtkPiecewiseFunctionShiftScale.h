/*=========================================================================

  Program:   Visualization Toolkit
  Module:    vtkPiecewiseFunctionShiftScale.h

  Copyright (c) Ken Martin, Will Schroeder, Bill Lorensen
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/Copyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/

// .NAME vtkPiecewiseFunctionShiftScale -
// 
// .SECTION Description

#ifndef __vtkPiecewiseFunctionShiftScale_h
#define __vtkPiecewiseFunctionShiftScale_h

#include "vtkPiecewiseFunctionAlgorithm.h"

class vtkPiecewiseFunction;

class VTK_FILTERING_EXPORT vtkPiecewiseFunctionShiftScale : public vtkPiecewiseFunctionAlgorithm
{
public:
  static vtkPiecewiseFunctionShiftScale *New();
  vtkTypeMacro(vtkPiecewiseFunctionShiftScale, vtkPiecewiseFunctionAlgorithm);
  void PrintSelf(ostream& os, vtkIndent indent);
  
  vtkSetMacro(PositionShift, BT_DOUBLE);
  vtkSetMacro(PositionScale, BT_DOUBLE);
  vtkSetMacro(ValueShift, BT_DOUBLE);
  vtkSetMacro(ValueScale, BT_DOUBLE);
  
  vtkGetMacro(PositionShift, BT_DOUBLE);
  vtkGetMacro(PositionScale, BT_DOUBLE);
  vtkGetMacro(ValueShift, BT_DOUBLE);
  vtkGetMacro(ValueScale, BT_DOUBLE);
  
protected:
  vtkPiecewiseFunctionShiftScale();
  ~vtkPiecewiseFunctionShiftScale();
  
  virtual int RequestData(vtkInformation *, vtkInformationVector **,
                          vtkInformationVector *);
  
  BT_DOUBLE PositionShift;
  BT_DOUBLE PositionScale;
  BT_DOUBLE ValueShift;
  BT_DOUBLE ValueScale;
  
private:
  vtkPiecewiseFunctionShiftScale(const vtkPiecewiseFunctionShiftScale&);  // Not implemented
  void operator=(const vtkPiecewiseFunctionShiftScale&);  // Not implemented
};

#endif
