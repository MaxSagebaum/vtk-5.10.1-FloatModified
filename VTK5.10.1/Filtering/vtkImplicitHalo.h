/*=========================================================================

  Program:   Visualization Toolkit
  Module:    vtkImplicitHalo.h

  Copyright (c) Ken Martin, Will Schroeder, Bill Lorensen
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/Copyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkImplicitHalo - implicit function for an halo
// .SECTION Description
// vtkImplicitHalo evaluates to 1.0 for each position in the sphere of a
// given center and radius Radius*(1-FadeOut). It evaluates to 0.0 for each
// position out the sphere of a given Center and radius Radius. It fades out
// linearly from 1.0 to 0.0 for points in a radius from Radius*(1-FadeOut) to
// Radius.
// vtkImplicitHalo is a concrete implementation of vtkImplicitFunction. 
// It is useful as an input to
// vtkSampleFunction to generate an 2D image of an halo. It is used this way by
// vtkShadowMapPass.
// .SECTION Caveats
// It does not implement the gradient.

#ifndef __vtkImplicitHalo_h
#define __vtkImplicitHalo_h

#include "vtkImplicitFunction.h"

class VTK_FILTERING_EXPORT vtkImplicitHalo : public vtkImplicitFunction
{
public:
  // Description
  // Center=(0.0,0.0,0.0), Radius=1.0, FadeOut=0.01
  static vtkImplicitHalo *New();

  vtkTypeMacro(vtkImplicitHalo,vtkImplicitFunction);
  void PrintSelf(ostream& os, vtkIndent indent);
  
  // Description
  // Evaluate the equation.
  virtual BT_DOUBLE EvaluateFunction(BT_DOUBLE x[3]);
  virtual BT_DOUBLE EvaluateFunction(BT_DOUBLE x, BT_DOUBLE y, BT_DOUBLE z)
    {
      return this->vtkImplicitFunction::EvaluateFunction(x, y, z);
    }

  // Description
  // Evaluate normal. Not implemented.
  void EvaluateGradient(BT_DOUBLE x[3], BT_DOUBLE g[3]);

  // Description:
  // Radius of the sphere.
  vtkSetMacro(Radius,BT_DOUBLE);
  vtkGetMacro(Radius,BT_DOUBLE);
  
  // Description:
  // Center of the sphere.
  vtkSetVector3Macro(Center,BT_DOUBLE);
  vtkGetVector3Macro(Center,BT_DOUBLE);
  
  // Description:
  // FadeOut ratio. Valid values are between 0.0 and 1.0.
  vtkSetMacro(FadeOut,BT_DOUBLE);
  vtkGetMacro(FadeOut,BT_DOUBLE);
  
protected:
  vtkImplicitHalo();
  virtual ~vtkImplicitHalo();

  BT_DOUBLE Radius;
  BT_DOUBLE Center[3];
  BT_DOUBLE FadeOut;

private:
  vtkImplicitHalo(const vtkImplicitHalo&); // Not implemented.
  void operator=(const vtkImplicitHalo&); // Not implemented.
};

#endif
