/*=========================================================================

  Program:   Visualization Toolkit
  Module:    vtkPointsProjectedHull.h

  Copyright (c) Ken Martin, Will Schroeder, Bill Lorensen
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/Copyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
/*----------------------------------------------------------------------------
 Copyright (c) Sandia Corporation
 See Copyright.txt or http://www.paraview.org/HTML/Copyright.html for details.
----------------------------------------------------------------------------*/

// .NAME vtkPointsProjectedHull - the convex hull of the orthogonal 
//    projection of the vtkPoints in the 3 coordinate directions
// .SECTION Description
//    a subclass of vtkPoints, it maintains the counter clockwise 
//    convex hull of the points (projected orthogonally in the 
//    three coordinate directions) and has a method to
//    test for intersection of that hull with an axis aligned
//    rectangle.  This is used for intersection tests of 3D volumes.

#ifndef __vtkPointsProjectedHull_h
#define __vtkPointsProjectedHull_h

#include "vtkPoints.h"

class VTK_FILTERING_EXPORT vtkPointsProjectedHull : public vtkPoints
{
    vtkTypeMacro(vtkPointsProjectedHull, vtkPoints);

public:
    void PrintSelf(ostream& os, vtkIndent indent);

    static vtkPointsProjectedHull *New();

    // Description:  Project the box R along the positive X axis and
    //   determine whether the resulting rectangle intersects the
    //   convex hull of the projection of the points along that axis.

    int RectangleIntersectionX(vtkPoints *R);

    // Description:  Determine whether the given rectangle intersects 
    //   the convex hull of the projection of the points along the
    //   positive X-axis.

    int RectangleIntersectionX(BT_FLOAT ymin, BT_FLOAT ymax, BT_FLOAT zmin, BT_FLOAT zmax);
    int RectangleIntersectionX(BT_DOUBLE ymin, BT_DOUBLE ymax, BT_DOUBLE zmin, BT_DOUBLE zmax);

    // Description:  Determine if a rectangle intersects the convex hull
    //   of the parallel projection along the Y axis of the points

    int RectangleIntersectionY(vtkPoints *R);

    // Description:  Determine whether the given rectangle intersects 
    //   the convex hull of the projection of the points along the
    //   positive Y-axis.

    int RectangleIntersectionY(BT_FLOAT zmin, BT_FLOAT zmax, BT_FLOAT xmin, BT_FLOAT xmax);
    int RectangleIntersectionY(BT_DOUBLE zmin, BT_DOUBLE zmax, BT_DOUBLE xmin, BT_DOUBLE xmax);

    // Description:  Determine if a rectangle intersects the convex hull
    //   of the parallel projection along the Z axis of the points

    int RectangleIntersectionZ(vtkPoints *R);

    // Description:  Determine whether the given rectangle intersects 
    //   the convex hull of the projection of the points along the
    //   positive Z-axis.

    int RectangleIntersectionZ(BT_FLOAT xmin, BT_FLOAT xmax, BT_FLOAT ymin, BT_FLOAT ymax);
    int RectangleIntersectionZ(BT_DOUBLE xmin, BT_DOUBLE xmax, BT_DOUBLE ymin, BT_DOUBLE ymax);

    // Description:
    //   Returns the coordinates (y,z) of the points in the convex hull 
    //   of the projection of the points down the positive x-axis.  pts has
    //   storage for len*2 values.
   
    int GetCCWHullX(BT_FLOAT *pts, int len);
    int GetCCWHullX(BT_DOUBLE *pts, int len);

    // Description:
    //   Returns the coordinates (z, x) of the points in the convex hull 
    //   of the projection of the points down the positive y-axis.  pts has
    //   storage for len*2 values.

    int GetCCWHullY(BT_FLOAT *pts, int len);
    int GetCCWHullY(BT_DOUBLE *pts, int len);

    // Description:
    //   Returns the coordinates (x, y) of the points in the convex hull 
    //   of the projection of the points down the positive z-axis.  pts has
    //   storage for len*2 values.

    int GetCCWHullZ(BT_FLOAT *pts, int len);
    int GetCCWHullZ(BT_DOUBLE *pts, int len);

    // Description:
    //  Returns the number of points in the convex hull of the projection
    //  of the points down the positive x-axis

    int GetSizeCCWHullX();

    // Description:
    //  Returns the number of points in the convex hull of the projection
    //  of the points down the positive y-axis

    int GetSizeCCWHullY();

    // Description:
    //  Returns the number of points in the convex hull of the projection
    //  of the points down the positive z-axis

    int GetSizeCCWHullZ();

    void Initialize();
    void Reset(){this->Initialize();}

    // Description:
    //   Forces recalculation of convex hulls, use this if
    //   you delete/add points

    void Update();

protected:

    vtkPointsProjectedHull();
    ~vtkPointsProjectedHull();

private:

  int RectangleIntersection(BT_DOUBLE hmin, BT_DOUBLE hmax,
                            BT_DOUBLE vmin, BT_DOUBLE vmax, int direction);
  int GrahamScanAlgorithm(int direction);
  void GetPoints();
  int RectangleBoundingBoxIntersection(BT_DOUBLE hmin, BT_DOUBLE hmax,
                            BT_DOUBLE vmin, BT_DOUBLE vmax, int direction);
  int RectangleOutside(BT_DOUBLE hmin, BT_DOUBLE hmax,
                            BT_DOUBLE vmin, BT_DOUBLE vmax, int direction);

  int RectangleOutside1DPolygon(BT_DOUBLE hmin, BT_DOUBLE hmax,
                            BT_DOUBLE vmin, BT_DOUBLE vmax, int dir);

  void InitFlags();
  void ClearAllocations();


  static int RemoveExtras(BT_DOUBLE *pts, int n);
  static BT_DOUBLE Distance(BT_DOUBLE *p1, BT_DOUBLE *p2);
  static int PositionInHull(BT_DOUBLE *base, BT_DOUBLE *top, BT_DOUBLE *pt);
  static int OutsideLine(BT_DOUBLE hmin, BT_DOUBLE hmax,
           BT_DOUBLE vmin, BT_DOUBLE vmax, BT_DOUBLE *p0, BT_DOUBLE *p1, BT_DOUBLE *insidePt);
  static int OutsideHorizontalLine(BT_DOUBLE vmin, BT_DOUBLE vmax, 
           BT_DOUBLE *p0, BT_DOUBLE *p1, BT_DOUBLE *insidePt);
  static int OutsideVerticalLine(BT_DOUBLE hmin, BT_DOUBLE hmax, BT_DOUBLE *p0, 
           BT_DOUBLE *p1, BT_DOUBLE *insidePt);

  BT_DOUBLE *Pts;
  int Npts;
  vtkTimeStamp PtsTime;

  BT_DOUBLE *CCWHull[3];
  BT_FLOAT HullBBox[3][4];
  int HullSize[3];
  vtkTimeStamp HullTime[3];

  vtkPointsProjectedHull(const vtkPointsProjectedHull&); // Not implemented
  void operator=(const vtkPointsProjectedHull&); // Not implemented
};
#endif


