/*=========================================================================

  Program:   Visualization Toolkit
  Module:    vtkQuadraticPyramid.h

  Copyright (c) Ken Martin, Will Schroeder, Bill Lorensen
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/Copyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkQuadraticPyramid - cell represents a parabolic, 13-node isoparametric pyramid
// .SECTION Description
// vtkQuadraticPyramid is a concrete implementation of vtkNonLinearCell to
// represent a three-dimensional, 13-node isoparametric parabolic
// pyramid. The interpolation is the standard finite element, quadratic
// isoparametric shape function. The cell includes a mid-edge node. The
// ordering of the thirteen points defining the cell is point ids (0-4,5-12)
// where point ids 0-4 are the five corner vertices of the pyramid; followed
// by eight midedge nodes (5-12). Note that these midedge nodes correspond lie
// on the edges defined by (0,1), (1,2), (2,3), (3,0), (0,4), (1,4), (2,4),
// (3,4).

// .SECTION See Also
// vtkQuadraticEdge vtkQuadraticTriangle vtkQuadraticTetra
// vtkQuadraticHexahedron vtkQuadraticQuad vtkQuadraticWedge

// .SECTION Thanks
// The shape functions and derivatives could be implemented thanks to
// the report Pyramid Solid Elements Linear and Quadratic Iso-P Models
// From Center For Aerospace Structures

#ifndef __vtkQuadraticPyramid_h
#define __vtkQuadraticPyramid_h

#include "vtkNonLinearCell.h"

class vtkQuadraticEdge;
class vtkQuadraticQuad;
class vtkQuadraticTriangle;
class vtkTetra;
class vtkPyramid;
class vtkDoubleArray;

class VTK_FILTERING_EXPORT vtkQuadraticPyramid : public vtkNonLinearCell
{
public:
  static vtkQuadraticPyramid *New();
  vtkTypeMacro(vtkQuadraticPyramid,vtkNonLinearCell);
  void PrintSelf(ostream& os, vtkIndent indent);

  // Description:
  // Implement the vtkCell API. See the vtkCell API for descriptions
  // of these methods.
  int GetCellType() {return VTK_QUADRATIC_PYRAMID;};
  int GetCellDimension() {return 3;}
  int GetNumberOfEdges() {return 8;}
  int GetNumberOfFaces() {return 5;}
  vtkCell *GetEdge(int edgeId);
  vtkCell *GetFace(int faceId);

  int CellBoundary(int subId, BT_DOUBLE pcoords[3], vtkIdList *pts);
  void Contour(BT_DOUBLE value, vtkDataArray *cellScalars,
               vtkIncrementalPointLocator *locator, vtkCellArray *verts,
               vtkCellArray *lines, vtkCellArray *polys,
               vtkPointData *inPd, vtkPointData *outPd,
               vtkCellData *inCd, vtkIdType cellId, vtkCellData *outCd);
  int EvaluatePosition(BT_DOUBLE x[3], BT_DOUBLE* closestPoint,
                       int& subId, BT_DOUBLE pcoords[3],
                       BT_DOUBLE& dist2, BT_DOUBLE *weights);
  void EvaluateLocation(int& subId, BT_DOUBLE pcoords[3], BT_DOUBLE x[3],
                        BT_DOUBLE *weights);
  int Triangulate(int index, vtkIdList *ptIds, vtkPoints *pts);
  void Derivatives(int subId, BT_DOUBLE pcoords[3], BT_DOUBLE *values,
                   int dim, BT_DOUBLE *derivs);
  virtual BT_DOUBLE *GetParametricCoords();

  // Description:
  // Clip this quadratic triangle using scalar value provided. Like
  // contouring, except that it cuts the triangle to produce linear
  // triangles.
  void Clip(BT_DOUBLE value, vtkDataArray *cellScalars,
            vtkIncrementalPointLocator *locator, vtkCellArray *tets,
            vtkPointData *inPd, vtkPointData *outPd,
            vtkCellData *inCd, vtkIdType cellId, vtkCellData *outCd,
            int insideOut);

  // Description:
  // Line-edge intersection. Intersection has to occur within [0,1] parametric
  // coordinates and with specified tolerance.
  int IntersectWithLine(BT_DOUBLE p1[3], BT_DOUBLE p2[3], BT_DOUBLE tol, BT_DOUBLE& t,
                        BT_DOUBLE x[3], BT_DOUBLE pcoords[3], int& subId);


  // Description:
  // Return the center of the quadratic pyramid in parametric coordinates.
  int GetParametricCenter(BT_DOUBLE pcoords[3]);

  // Description:
  // @deprecated Replaced by vtkQuadraticPyramid::InterpolateFunctions as of VTK 5.2
  static void InterpolationFunctions(BT_DOUBLE pcoords[3], BT_DOUBLE weights[13]);
  // Description:
  // @deprecated Replaced by vtkQuadraticPyramid::InterpolateDerivs as of VTK 5.2
  static void InterpolationDerivs(BT_DOUBLE pcoords[3], BT_DOUBLE derivs[39]);
  // Description:
  // Compute the interpolation functions/derivatives
  // (aka shape functions/derivatives)
  virtual void InterpolateFunctions(BT_DOUBLE pcoords[3], BT_DOUBLE weights[13])
    {
    vtkQuadraticPyramid::InterpolationFunctions(pcoords,weights);
    }
  virtual void InterpolateDerivs(BT_DOUBLE pcoords[3], BT_DOUBLE derivs[39])
    {
    vtkQuadraticPyramid::InterpolationDerivs(pcoords,derivs);
    }
  // Description:
  // Return the ids of the vertices defining edge/face (`edgeId`/`faceId').
  // Ids are related to the cell, not to the dataset.
  static int *GetEdgeArray(int edgeId);
  static int *GetFaceArray(int faceId);

  // Description:
  // Given parametric coordinates compute inverse Jacobian transformation
  // matrix. Returns 9 elements of 3x3 inverse Jacobian plus interpolation
  // function derivatives.
  void JacobianInverse(BT_DOUBLE pcoords[3], BT_DOUBLE **inverse, BT_DOUBLE derivs[39]);

protected:
  vtkQuadraticPyramid();
  ~vtkQuadraticPyramid();

  vtkQuadraticEdge *Edge;
  vtkQuadraticTriangle *TriangleFace;
  vtkQuadraticQuad *Face;
  vtkTetra         *Tetra;
  vtkPyramid       *Pyramid;
  vtkPointData     *PointData;
  vtkCellData      *CellData;
  vtkDoubleArray   *CellScalars;
  vtkDoubleArray   *Scalars; //used to avoid New/Delete in contouring/clipping

  void Subdivide(vtkPointData *inPd, vtkCellData *inCd, vtkIdType cellId,
    vtkDataArray *cellScalars);

private:
  vtkQuadraticPyramid(const vtkQuadraticPyramid&);  // Not implemented.
  void operator=(const vtkQuadraticPyramid&);  // Not implemented.
};
//----------------------------------------------------------------------------
// Return the center of the quadratic pyramid in parametric coordinates.
//
inline int vtkQuadraticPyramid::GetParametricCenter(BT_DOUBLE pcoords[3])
{
  pcoords[0] = pcoords[1] = 6./13;
  pcoords[2] = 3./13;
  return 0;
}


#endif
