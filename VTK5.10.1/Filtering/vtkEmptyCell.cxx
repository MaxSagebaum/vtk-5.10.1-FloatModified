/*=========================================================================

  Program:   Visualization Toolkit
  Module:    vtkEmptyCell.cxx

  Copyright (c) Ken Martin, Will Schroeder, Bill Lorensen
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/Copyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
#include "vtkEmptyCell.h"

#include "vtkCellArray.h"
#include "vtkMath.h"
#include "vtkObjectFactory.h"
#include "vtkPoints.h"

vtkStandardNewMacro(vtkEmptyCell);

//----------------------------------------------------------------------------
int vtkEmptyCell::EvaluatePosition(BT_DOUBLE  vtkNotUsed(x)[3],
                                  BT_DOUBLE  vtkNotUsed(closestPoint)[3],
                                  int&   vtkNotUsed(subId),
                                  BT_DOUBLE  vtkNotUsed(pcoords)[3],
                                  BT_DOUBLE& vtkNotUsed(dist2),
                                  BT_DOUBLE  *vtkNotUsed(weights))
{
  return 0;
}

//----------------------------------------------------------------------------
void vtkEmptyCell::EvaluateLocation(int&  vtkNotUsed(subId),
                                   BT_DOUBLE vtkNotUsed(pcoords)[3],
                                   BT_DOUBLE vtkNotUsed(x)[3],
                                   BT_DOUBLE *vtkNotUsed(weights))
{
}

//----------------------------------------------------------------------------
int vtkEmptyCell::CellBoundary(int vtkNotUsed(subId),
                            BT_DOUBLE vtkNotUsed(pcoords)[3],
                            vtkIdList *vtkNotUsed(pts))
{
  return 0;
}

//----------------------------------------------------------------------------
void vtkEmptyCell::Contour(BT_DOUBLE vtkNotUsed(value),
                           vtkDataArray *vtkNotUsed(cellScalars),
                           vtkIncrementalPointLocator *vtkNotUsed(locator),
                           vtkCellArray *vtkNotUsed(verts),
                           vtkCellArray *vtkNotUsed(lines),
                           vtkCellArray *vtkNotUsed(polys),
                           vtkPointData *vtkNotUsed(inPd),
                           vtkPointData *vtkNotUsed(outPd),
                           vtkCellData *vtkNotUsed(inCd),
                           vtkIdType vtkNotUsed(cellId),
                           vtkCellData *vtkNotUsed(outCd))
{
}

//----------------------------------------------------------------------------
// Project point on line. If it lies between 0<=t<=1 and distance off line
// is less than tolerance, intersection detected.
int vtkEmptyCell::IntersectWithLine(BT_DOUBLE vtkNotUsed(p1)[3],
                                   BT_DOUBLE vtkNotUsed(p2)[3],
                                   BT_DOUBLE vtkNotUsed(tol),
                                   BT_DOUBLE& vtkNotUsed(t),
                                   BT_DOUBLE vtkNotUsed(x)[3],
                                   BT_DOUBLE pcoords[3],
                                   int& vtkNotUsed(subId))
{
  pcoords[0] = -10.0;
  return 0;
}

//----------------------------------------------------------------------------
int vtkEmptyCell::Triangulate(int vtkNotUsed(index),
                             vtkIdList *ptIds, vtkPoints *pts)
{
  pts->Reset();
  ptIds->Reset();

  return 1;
}

//----------------------------------------------------------------------------
void vtkEmptyCell::Derivatives(int vtkNotUsed(subId),
                            BT_DOUBLE vtkNotUsed(pcoords)[3],
                            BT_DOUBLE *vtkNotUsed(values),
                            int vtkNotUsed(dim),
                            BT_DOUBLE *vtkNotUsed(derivs))
{
}

//----------------------------------------------------------------------------
void vtkEmptyCell::Clip(BT_DOUBLE vtkNotUsed(value),
                        vtkDataArray *vtkNotUsed(cellScalars),
                        vtkIncrementalPointLocator *vtkNotUsed(locator),
                        vtkCellArray *vtkNotUsed(verts),
                        vtkPointData *vtkNotUsed(inPD),
                        vtkPointData *vtkNotUsed(outPD),
                        vtkCellData *vtkNotUsed(inCD),
                        vtkIdType vtkNotUsed(cellId),
                        vtkCellData *vtkNotUsed(outCD),
                        int vtkNotUsed(insideOut))
{
}

//----------------------------------------------------------------------------
void vtkEmptyCell::InterpolateFunctions(BT_DOUBLE pcoords[3], BT_DOUBLE *weights)
{
  (void)pcoords;
  (void)weights;
}

//----------------------------------------------------------------------------
void vtkEmptyCell::InterpolateDerivs(BT_DOUBLE pcoords[3], BT_DOUBLE *derivs)
{
  (void)pcoords;
  (void)derivs;
}

//----------------------------------------------------------------------------
void vtkEmptyCell::PrintSelf(ostream& os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os,indent);
}
